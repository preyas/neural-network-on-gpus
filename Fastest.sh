#!/usr/bin/env bash

#SBATCH --job-name=opt
#SBATCH --output=OP/all-%j.out
#SBATCH --error=OP/all-%j.err
#SBATCH -p cme213
#SBATCH --tasks=6
#SBATCH --gres=gpu:6

# data generation
mpirun -np 3 ./main -n 800 -l 0.1 -e 12 -b 1500 -o
mpirun -np 4 ./main -n 800 -l 0.1 -e 12	-b 1500 -o
mpirun -np 3 ./main -n 800 -l 0.095 -e 11 -b 1500 -o
