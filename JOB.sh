#!/usr/bin/env bash

#SBATCH --job-name=opt
#SBATCH --output=OP/all-%j.out
#SBATCH --error=OP/all-%j.err
#SBATCH -p cme213
#SBATCH --tasks=6
#SBATCH --gres=gpu:6

# Reminder: don't forget to load the right modules:
# $ module add shared cuda65 openmpi/gcc/64/1.8.1

# profiled output
mpirun -np 1 nvprof --output-profile ser.out.%p ./main -n 1000 -e 20 -b 800 -s 1
mpirun nvprof --output-profile par.out.%p ./main -n 1000 -e 20 -b 800 -p 1
mpirun nvprof --output-profile opt.out.%p ./main -n 1000 -e 20 -b 800 -o

# without nvprof overhead
mpirun -np 1 ./main -n 1000 -e 20 -b 800 -s 1
mpirun ./main -n 1000 -e 20 -b 800 -p 1
mpirun ./main -n 1000 -e 20 -b 800 -o

# to compile without Rank 0 processor info, comment out #define _prof_mpi in defs.h, compile and run this script again
