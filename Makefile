CC=g++
CUD=nvcc
CFLAGS=-std=c++11 -fpermissive -pthread -I./lib/usr/include -I/cm/shared/apps/openmpi/gcc/64/1.8.1/include 
LFLAGS=-larmadillo -lcublas -lcudart -lmpi_cxx -lmpi -pthread -L=/cm/shared/apps/openmpi/gcc/64/1.8.1/lib64 -L=./lib/usr/lib64 -L=/cm/shared/apps/cuda65/toolkit/6.5.14/lib64 -Wl,-rpath=/opt/intel/composer_xe_2013_sp1.0.080/mkl/lib/intel64/,-rpath=./lib/usr/lib64,-rpath=/cm/shared/apps/openmpi/gcc/64/1.8.1/lib64
CUDFLAGS=-O3 -c -arch=sm_20 -Xcompiler -Wall,-Winline,-Wextra,-Wno-strict-aliasing,-Wno-unused-parameter

main: main.o two_layer_net.o mnist.o common.o simple_mat.o gpu_func.o
	$(CC) $(LFLAGS) main.o two_layer_net.o mnist.o common.o simple_mat.o gpu_func.o -o main

main.o: main.cpp utils/test_utils.h defs.h simple_mat.h gpu_func.h utils/common.h
	$(CC) $(CFLAGS) -c main.cpp

simple_mat.o: simple_mat.cpp simple_mat.h defs.h gpu_func.h
	$(CC) $(CFLAGS) -c simple_mat.cpp

two_layer_net.o: utils/two_layer_net.cpp defs.h simple_mat.h
	$(CC) $(CFLAGS) -c utils/two_layer_net.cpp

mnist.o: utils/mnist.cpp
	$(CC) $(CFLAGS) -c utils/mnist.cpp

common.o: utils/common.cpp
	$(CC) $(CFLAGS) -c utils/common.cpp

gpu_func.o: gpu_func.cu defs.h gpu_func.h
	$(CUD) $(CUDFLAGS) -c gpu_func.cu

clean:
	rm -rf *.o main

