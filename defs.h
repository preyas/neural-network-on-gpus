/*
 * =====================================================================================
 *
 *       Filename:  defs.h
 *
 *    Description:  definitions that may be used everywhere
 *
 *        Version:  1.0
 *        Created:  05/28/2015 01:33:43 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Preyas Shah (), 
 *   Organization:  Stanford University
 *
 * =====================================================================================
 */

#ifndef _DEFS_H
#define _DEFS_H

/* ----- main ----------*/
#define _main_1

/* ----- unit tests ----------*/
#ifndef _main_1
	#define _test_gemm
	//#define _test_mat
#endif

// uncomment the following for various functionalities in the code

//#define _prof_mpi
//#define _prof_gpu
//#define _debug
//#define _check

#endif
