#include "gpu_func.h"
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>
#include "vector_functions.h"	
#include <iostream>

// naive computation. Note A,B,C,D are column-major order
__global__ void GEMM_Jun1(double alpha, double* A, double* B, double beta, double* C,
		double* D, const int An, const int Cnc, const int Cnr, const int type){

	// map threads to 2D matrix
	int ix = threadIdx.x + blockIdx.x*blockDim.x;
	int iy = threadIdx.y + blockIdx.y*blockDim.y;
	int idx = iy + ix*Cnr;
	
	// for threads mapped to locations outside of C, return
	if(iy>=Cnr || ix>=Cnc)
		return;
		
	double sum = 0.;
	if(type==0){
	// loop over to compute D = alpha * A * B^T + beta * C
		for(int j=0; j<An; j++)
			sum += A[j*Cnr + iy]*B[j*Cnc+ix];
		D[idx] = alpha*sum+ beta*C[idx];
	}
	if(type==1){
	// loop over to compute D = alpha * A^ * B + beta * C
		for(int j=0; j<An; j++)
			sum += A[iy*An + j]*B[ix*An+j];
		D[idx] = alpha*sum+ beta*C[idx];	
	}
	if(type==2){
	// loop over to compute D = alpha * (A * B) o  C o (beta - C)
		for(int j=0; j<An; j++)
			sum += A[j*Cnr + iy]*B[ix*An+j];
		D[idx] = alpha*sum*C[idx]*(beta-C[idx]);
	}
	
	return;
}

void DoGEMM_Jun1(double* Z, double* A, double* W, double* b, double alpha, double beta, int Bnr, int Bnc, int Anc, int type){

	int threadsPerBlock = 256;
	dim3 threads(1,1);
	threads.x = 16;
	threads.y = (threadsPerBlock+threads.x-1)/threads.x;

	dim3 blocks(1,1);
	blocks.x = (Bnc + threads.x-1)/threads.x;
	blocks.y = (Bnr + threads.y-1)/threads.y;

	double* dev_Z = 0;
	double* dev_A = 0;
	double* dev_W = 0;
	double* dev_b = 0;

	// cuda mallocs
	checkCudaErrors(cudaMalloc((void**)&dev_Z, Bnr*Bnc*sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&dev_A, Bnr*Anc*sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&dev_W, Anc*Bnc*sizeof(double)));
	checkCudaErrors(cudaMalloc((void**)&dev_b, Bnr*Bnc*sizeof(double)));

	// cuda memcopy H2D
	checkCudaErrors(cudaMemcpy(dev_A,A,Bnr*Anc*sizeof(double),cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(dev_W,W,Anc*Bnc*sizeof(double),cudaMemcpyHostToDevice));
	checkCudaErrors(cudaMemcpy(dev_b,b,Bnr*Bnc*sizeof(double),cudaMemcpyHostToDevice));

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	GEMM_Jun1<<<blocks,threads>>>(alpha,dev_A,dev_W,beta,dev_b,dev_Z,Anc,Bnc,Bnr,type);
	#ifdef _check
	check_launch ("GEMM_Jun1");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "GEMM_Jun1 call took: " << time << " seconds" << std::endl;
	#endif

	// cuda memcopy D2H
	checkCudaErrors(cudaMemcpy(Z,dev_Z,Bnr*Bnc*sizeof(double),cudaMemcpyDeviceToHost));

	// cuda Frees
	checkCudaErrors(cudaFree(dev_A));
	checkCudaErrors(cudaFree(dev_W));
	checkCudaErrors(cudaFree(dev_b));
	checkCudaErrors(cudaFree(dev_Z));
	
	cudaDeviceSynchronize();
}


/* **************************************************************************** */
/* ************************ OPTIMIZATED  ROUTINES ***************************** */
/* **************************************************************************** */


/*-----------------------------------------------------------------------
			Memory operations
------------------------------------------------------------------------*/
__global__
void _filldouble_(double* mat, const int size, const double val){
	int tid = threadIdx.x + blockDim.x*(blockIdx.x+gridDim.x*blockIdx.y);
	if(tid>=size)
		return;
	mat[tid] = val;
}

void FillLinDevMem(double* mat, const int size, const double val){
	dim3 threads(32,1);
	int x = ceil(sqrt((size + threads.x - 1)/threads.x));
	dim3 blocks(x,x);

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif
	
	_filldouble_<<<blocks,threads>>>(mat, size, val);
	#ifdef _check
	check_launch ("_filldouble_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "FillDouble call took: " << time << " seconds" << std::endl;
	#endif

//	cudaDeviceSynchronize();
}

void AllocFillLinDevMem(double*& mat, const int size, const double val){
	checkCudaErrors(cudaMalloc((void**)&mat, size*sizeof(double)));
	FillLinDevMem(mat,size,val);
}

void AllocLinDevMem(double*& mat, const int size){
	checkCudaErrors(cudaMalloc((void**)&mat, size*sizeof(double)));
}

void AllocLinHostMem(double*& mat, const int size, bool pinned){
	if(pinned)
		checkCudaErrors(cudaHostAlloc(&mat,size*sizeof(double),cudaHostAllocMapped));
	else
		mat = new double[size];
}

void DeallocLinHostMem(double*& mat, bool pinned){
	if(pinned)
		checkCudaErrors(cudaFreeHost(mat));
	else
		delete [] mat;
}

void DeallocLinDevMem(double*& mat){
	checkCudaErrors(cudaFree(mat));
}

void Copy(double*& to, const double* from, const char src, const char dst, int size, char sync, cudaStream_t* stream) {
	#ifdef _check
	assert(src=='h' or src=='d');
	assert(dst=='h' or dst=='d');
	assert(sync=='a' or sync=='s');
	#endif

	if(src==dst){
		if(src=='h'){
			memcpy(to,from,size*sizeof(double));
		}
		if(src=='d'){
			checkCudaErrors(cudaMemcpy(to,from,size*sizeof(double),cudaMemcpyDeviceToDevice));
		}
	}else{
		if(src=='d'){
			if(sync=='s')
				checkCudaErrors(cudaMemcpy(to,from,size*sizeof(double),cudaMemcpyDeviceToHost));
			else{
				if(stream)
					checkCudaErrors(cudaMemcpyAsync(to,from,size*sizeof(double),cudaMemcpyDeviceToHost,*stream));
				else
					checkCudaErrors(cudaMemcpyAsync(to,from,size*sizeof(double),cudaMemcpyDeviceToHost));
			}
		}
		if(src=='h'){
			if(sync=='s')
				checkCudaErrors(cudaMemcpy(to,from,size*sizeof(double),cudaMemcpyHostToDevice));
			else{
				if(stream)
					checkCudaErrors(cudaMemcpyAsync(to,from,size*sizeof(double),cudaMemcpyHostToDevice,*stream));
				else
					checkCudaErrors(cudaMemcpyAsync(to,from,size*sizeof(double),cudaMemcpyHostToDevice));
			}
		}
	}
//	cudaDeviceSynchronize();
}

/*-----------------------------------------------------------------------
			memory access routines
------------------------------------------------------------------------*/
__global__
void _getval_(double* mat, int idx, double* getval){
	*getval = mat[idx];
}

__global__
void _setval_(double* mat, int idx, const double setval){
	mat[idx] = setval;	
}

double Getval(double*& mat, const int idx){
	double* d_val;	
	double getval;
	checkCudaErrors (cudaMalloc((void **)&d_val, 1 * sizeof (double)));

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	_getval_<<<1,1>>>(mat,idx,d_val);
	#ifdef _check
	check_launch ("_getval_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "Getval call took: " << time << " seconds" << std::endl;
	#endif

	checkCudaErrors(cudaMemcpy(&getval,d_val,sizeof(double),cudaMemcpyDeviceToHost));
	checkCudaErrors (cudaFree(d_val));
	cudaDeviceSynchronize();
	return getval;
}

void Setval(double*& mat, const int idx, const double setval){
	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	_setval_<<<1,1>>>(mat,idx,setval);
	#ifdef _check
	check_launch ("_setval_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "SetVal call took: " << time << " seconds" << std::endl;
	#endif

	cudaDeviceSynchronize();
}

/*-----------------------------------------------------------------------
			Matrix ops
------------------------------------------------------------------------*/
// type 0: Z = alpha * A * W^T + beta * b
// type 1: Z = alpha * A^T * W + beta * b
// type 2: Z = alpha * (A * W) o b o (beta - b)
__global__ void _gemm_naive_(const double alpha, const double* A, const double* B, const double beta, const double* C, double* D, const int Anr, const int Anc, const int Bnr, const int Bnc, const int Cnr, const int Cnc, const int type){

	// map threads to 2D matrix
	int ix = threadIdx.x + blockIdx.x*blockDim.x;
	int iy = threadIdx.y + blockIdx.y*blockDim.y;
	int idx = ix + iy*Cnc;
	
	// for threads mapped to locations outside of C, return
	if(iy>=Cnr || ix>=Cnc)
		return;
		
	double sum = 0.;
	if(type==0){
	// loop over to compute D = alpha * A * B^T + beta * C
		for(int j=0; j<Anc; j++)
			sum += A[iy*Anc + j]*B[ix*Bnc+j];
		D[idx] = alpha*sum+ beta*C[idx];
	}
	if(type==1){
	// loop over to compute D = alpha * A^ * B + beta * C
		for(int j=0; j<Bnr; j++)
			sum += A[j*Anc + iy]*B[j*Bnc+ix];
		D[idx] = alpha*sum+ beta*C[idx];
	}
	if(type==2){
	// loop over to compute D = alpha * (A * B) o  C o (beta - C)
		for(int j=0; j<Anc; j++)
			sum += A[iy*Anc + j]*B[j*Bnc+ix];
		D[idx] = alpha*sum*C[idx]*(beta-C[idx]);
	}
	
}

__global__ void _gemm_opt_(const double alpha, const double* A, const double* B, const double beta, const double* C, double* D, const int Anr, const int Anc, const int Bnr, const int Bnc, const int Cnr, const int Cnc, const int type){

	int stride = 16;// 0.5warp

	__shared__ double smemA[16][17]; //blockDim.y x stride
	__shared__ double smemB[16][17]; //blockDim.x x stride

	int C_tidx = threadIdx.x + blockIdx.x*blockDim.x;
	int C_tidy = threadIdx.y + blockIdx.y*blockDim.y;

	double sum =0;

	// TODO can be optimized further
	if(type==0){ // Z = a*A*B^T + b*C
		int numK = (Anc+stride-1)/stride; // = Bnc/B_bdimx
		for(int K = 0; K< numK; K++){
			// load A(blockIdx.y, K) [blockDimy x stride]
			int A_tidx = threadIdx.x + K*stride;
			int A_tidy = threadIdx.y + blockIdx.y*stride;
			if(A_tidx<Anc && A_tidy<Anr)
				smemA[threadIdx.x][threadIdx.y] = A[A_tidx + A_tidy*Anc];
			else
				smemA[threadIdx.x][threadIdx.y] = 0.;
			__syncthreads();
			// load B(blockIdx.x, K) [blockDimx x stride]
			int B_tidx = threadIdx.x + K*stride;
			int B_tidy = threadIdx.y + blockIdx.x*stride;
			if(B_tidx<Bnc && B_tidy<Bnr)
				smemB[threadIdx.x][threadIdx.y] = B[B_tidx + B_tidy*Anc];
			else
				smemB[threadIdx.x][threadIdx.y] = 0.;
			__syncthreads();
			if(C_tidx<Cnc && C_tidy<Cnr){
				for(int k = 0; k<stride; k++)
					sum += smemA[k][threadIdx.y]*smemB[k][threadIdx.x];
			}
			__syncthreads();
		}
	}
	if(type==1){ // Z = a*A^T*B +b*C
		int numK = (Anr+stride-1)/stride; // = Bnr/B_bdimy
		for(int K = 0; K< numK; K++){
			// load A
			int A_tidx = threadIdx.x + blockIdx.y*stride;
			int A_tidy = threadIdx.y + K*stride;
			if(A_tidx<Anc && A_tidy<Anr)
				smemA[threadIdx.y][threadIdx.x] = A[A_tidx + A_tidy*Anc];
			else
				smemA[threadIdx.y][threadIdx.x] = 0.;
			__syncthreads();
			// load B
			int B_tidx = threadIdx.x + blockIdx.x*stride;
			int B_tidy = threadIdx.y + K*stride;
			if(B_tidx<Bnc && B_tidy<Bnr)
				smemB[threadIdx.y][threadIdx.x] = B[B_tidx + B_tidy*Bnc];
			else
				smemB[threadIdx.y][threadIdx.x] = 0.;
			__syncthreads();
			if(C_tidx<Cnc && C_tidy<Cnr){
				for(int k = 0; k<stride; k++)
					sum += smemA[k][threadIdx.y]*smemB[k][threadIdx.x];
			}
			__syncthreads();
		}
	}
	if(type==2){ // Z = a*A*B + b*C
		int numK = (Anc+stride-1)/stride; // = Bnr/B_bdimy
		for(int K = 0; K< numK; K++){
			// load A
			int A_tidx = threadIdx.x + K*stride;
			int A_tidy = threadIdx.y + blockIdx.y*stride;
			if(A_tidx<Anc && A_tidy<Anr)
				smemA[threadIdx.x][threadIdx.y] = A[A_tidx + A_tidy*Anc];
			else
				smemA[threadIdx.x][threadIdx.y] = 0.;
			__syncthreads();
			// load B
			int B_tidx = threadIdx.x + blockIdx.x*stride;
			int B_tidy = threadIdx.y + K*stride;
			if(B_tidx<Bnc && B_tidy<Bnr)
				smemB[threadIdx.y][threadIdx.x] = B[B_tidx + B_tidy*Bnc];
			else
				smemB[threadIdx.y][threadIdx.x] = 0.;
			__syncthreads();
			if(C_tidx<Cnc && C_tidy<Cnr){
				for(int k = 0; k<stride; k++)
					sum += smemA[k][threadIdx.y]*smemB[k][threadIdx.x];
			}
			__syncthreads();
		}
	}
	if(C_tidx<Cnc && C_tidy<Cnr){
		if(type!=2)
			D[C_tidx + C_tidy*Cnc] = alpha*sum + beta*C[C_tidx + C_tidy*Cnc];
		else
			D[C_tidx + C_tidy*Cnc] = alpha*sum*C[C_tidx + C_tidy*Cnc]*(beta-C[C_tidx + C_tidy*Cnc]);
	}
}

__global__ void _gemm_opt2_(const double alpha, const double* A, const double* B, const double beta, const double* C, double* D, const int Anr, const int Anc, const int Bnr, const int Bnc, const int Cnr, const int Cnc, const int type){

	double regC[16] = {0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.};
	double regB[4] = {0.,0.,0.,0.};
	int numK, A_tidx, A_tidy, B_tidx, B_tidy;
	if(type==0){
		__shared__ double smemA[16][4];
		numK = (Anc+3)/4;
		B_tidy = (threadIdx.x+threadIdx.y*16)+blockIdx.x*64;
		A_tidy = threadIdx.x + blockIdx.y*16;
		for(int K = 0; K< numK; K++){
			// load B
			for(int i = 0; i<4; i++){
				B_tidx = i + 4*K;
				if(B_tidx<Bnc && B_tidy<Bnr)
					regB[i] = B[B_tidx + B_tidy*Bnc];
			}
			__syncthreads();
			// load A
			A_tidx = threadIdx.y + 4*K;
			if(A_tidx<Anc && A_tidy<Anr)
				smemA[threadIdx.x][threadIdx.y] = A[A_tidx + A_tidy*Anc];
			else
				smemA[threadIdx.x][threadIdx.y] = 0.;
			__syncthreads();
			// update D
			for(int i = 0; i<16; i++){
				for(int k = 0; k<4; k++)
					regC[i] += regB[k]*smemA[i][k];
			}
			__syncthreads();	
		}
	}
	if(type==1){
		__shared__ double smemA[16][4];
		numK = (Anr+3)/4;
		B_tidx = (threadIdx.x+threadIdx.y*16)+blockIdx.x*64;
		A_tidx = threadIdx.x + blockIdx.y*16;
		for(int K = 0; K< numK; K++){
			// load B
			for(int i = 0; i<4; i++){
				B_tidy = i + 4*K;
				if(B_tidx<Bnc && B_tidy<Bnr)
					regB[i] = B[B_tidx + B_tidy*Bnc];
			}
			__syncthreads();
			// load A
			A_tidy = threadIdx.y + 4*K;
			if(A_tidx<Anc && A_tidy<Anr)
				smemA[threadIdx.x][threadIdx.y] = A[A_tidx + A_tidy*Anc];
			else
				smemA[threadIdx.x][threadIdx.y] = 0.;
			__syncthreads();
			// update D
			for(int i = 0; i<16; i++){
				for(int k = 0; k<4; k++)
					regC[i] += regB[k]*smemA[i][k];
			}
			__syncthreads();	
		}
	}
	if(type==2){
		__shared__ double smemA[4][17];
		numK = (Bnr+3)/4;
		B_tidx = (threadIdx.x+threadIdx.y*16)+blockIdx.x*64;
		A_tidy = threadIdx.x + blockIdx.y*16;
		for(int K = 0; K< numK; K++){
			// load B
			for(int i = 0; i<4; i++){
				B_tidy = i + 4*K;
				if(B_tidx<Bnc && B_tidy<Bnr)
					regB[i] = B[B_tidx + B_tidy*Bnc];
			}
			__syncthreads();
			// load A
			A_tidx = threadIdx.y + 4*K;
			if(A_tidx<Anc && A_tidy<Anr)
				smemA[threadIdx.y][threadIdx.x] = A[A_tidx + A_tidy*Anc];
			else
				smemA[threadIdx.y][threadIdx.x] = 0.;
			__syncthreads();
			// update D
			for(int i = 0; i<16; i++){
				for(int k = 0; k<4; k++)
					regC[i] += regB[k]*smemA[k][i];
			}
			__syncthreads();	
		}
	}
	for(int i =0; i<16; i++){
		int C_tidy = i+16*blockIdx.y;
		int C_tidx = (threadIdx.x+threadIdx.y*16)+blockIdx.x*64;
		if(C_tidx<Cnc && C_tidy<Cnr){
			if(type!=2)
				D[C_tidx+C_tidy*Cnc] = alpha*regC[i] + beta*C[C_tidx + C_tidy*Cnc];
			else
				D[C_tidx+C_tidy*Cnc] = alpha*regC[i]* C[C_tidx + C_tidy*Cnc]*(beta-C[C_tidx+C_tidy*Cnc]);
		}
	}
}

void _dev_gemm_(double* Z, const double* A, const double* W, const double* b, const double alpha, const double beta, const int Anr, const int Anc,const int Wnr, const int Wnc, const int Bnr, const int Bnc,const int type, cudaStream_t* stream){

	dim3 threads;
	dim3 blocks;
	if(type==1){
		dim3 threads(16,4);
		dim3 blocks(1,1);
		blocks.x = (Bnc + 64 -1)/64;
		blocks.y = (Bnr + 16-1)/16;

		#ifdef _prof_gpu
		event_pair timer;
		start_timer (&timer);
		#endif

		if(!stream)
			_gemm_opt2_<<<blocks,threads>>>(alpha,A,W,beta,b,Z,Anr,Anc,Wnr,Wnc,Bnr,Bnc,type);
		else
			_gemm_opt2_<<<blocks,threads,0,*stream>>>(alpha,A,W,beta,b,Z,Anr,Anc,Wnr,Wnc,Bnr,Bnc,type);
			
		#ifdef _check
		check_launch ("_gemm_");
		#endif

		#ifdef _prof_gpu
		double time = stop_timer (&timer);
		std::cout << "GEMM call took: " << time << " seconds" << std::endl;
		#endif
	}
	else{
		dim3 threads(16,16);
		dim3 blocks(1,1);
		blocks.x = (Bnc + threads.x-1)/threads.x;
		blocks.y = (Bnr + threads.y-1)/threads.y;


		#ifdef _prof_gpu
		event_pair timer;
		start_timer (&timer);
		#endif

		if(!stream)
			_gemm_opt_<<<blocks,threads>>>(alpha,A,W,beta,b,Z,Anr,Anc,Wnr,Wnc,Bnr,Bnc,type);
		else
			_gemm_opt_<<<blocks,threads,0,*stream>>>(alpha,A,W,beta,b,Z,Anr,Anc,Wnr,Wnc,Bnr,Bnc,type);
			
		#ifdef _check
		check_launch ("_gemm_");
		#endif

		#ifdef _prof_gpu
		double time = stop_timer (&timer);
		std::cout << "GEMM call took: " << time << " seconds" << std::endl;
		#endif
	}
	//cudaDeviceSynchronize();
}

// always column-wise reduce and scatter
__global__
void _reduce_(const double* mat, const int nr, const int nc, double* rowvec){

	int xid = threadIdx.x + blockIdx.x*blockDim.x;
	if(xid>=nc)
		return;
	
	rowvec[xid] = 0;
	for(int i = 0; i<nr; i++)
		rowvec[xid] += mat[i*nc + xid];
	
}

void _dev_reduce_(const double* mat, const int nr, const int nc, double* rowvec, cudaStream_t* stream){
	int threads = 128;
	int blocks = (nc+threads-1)/threads;

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	if(!stream)
		_reduce_<<<blocks,threads>>>(mat,nr,nc,rowvec);
	else
		_reduce_<<<blocks,threads,0,*stream>>>(mat,nr,nc,rowvec);
		
	#ifdef _check
	check_launch ("_reduce_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "Reduce call took: " << time << " seconds" << std::endl;
	#endif
	
	//cudaDeviceSynchronize();

}

__global__
void _repmat_(double* mat, const int nr, const int nc, const double* rowvec){

	int xid = threadIdx.x + blockIdx.x*blockDim.x;
	if(xid>=nc)
		return;
	
	for(int i = 0; i<nr; i++)
		mat[i*nc + xid] = rowvec[xid];
}

void _dev_repmat_(double* mat, const int nr, const int nc, const double* rowvec, cudaStream_t* stream){
	int threads = 128;
	int blocks = (nc+threads-1)/threads;

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	if(!stream)
		_repmat_<<<blocks,threads>>>(mat,nr,nc,rowvec);
	else
		_repmat_<<<blocks,threads,0,*stream>>>(mat,nr,nc,rowvec);

	#ifdef _check
	check_launch ("_repmat_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "Repmat call took: " << time << " seconds" << std::endl;
	#endif
	
	//cudaDeviceSynchronize();

}

__global__
void _axpby_(double* Z, const double alpha, const double* X, const double beta, const double* Y, const int nr, const int nc){
	// map threads to 2D matrix
	int ix = threadIdx.x + blockIdx.x*blockDim.x;
	int iy = threadIdx.y + blockIdx.y*blockDim.y;
	int idx = ix + iy*nc;
	
	// for threads mapped to locations outside of matrix, return
	if(iy>=nr || ix>=nc)
		return;
	
	Z[idx] = alpha*X[idx] + beta*Y[idx];
}

void _dev_axpby_(double *Z, const double alpha, const double* X, const double beta,const double* Y, const int nr, const int nc, cudaStream_t* stream){
	int threadsPerBlock = 256;
	dim3 threads(1,1);
	threads.x = 16;
	threads.y = (threadsPerBlock+threads.x-1)/threads.x;

	dim3 blocks(1,1);
	blocks.x = (nc + threads.x-1)/threads.x;
	blocks.y = (nr + threads.y-1)/threads.y;

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	if(!stream)
		_axpby_<<<blocks,threads>>>(Z,alpha,X,beta,Y,nr,nc);
	else
		_axpby_<<<blocks,threads,0,*stream>>>(Z,alpha,X,beta,Y,nr,nc);

	#ifdef _check
	check_launch ("_axpby_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "AXPBY call took: " << time << " seconds" << std::endl;
	#endif
	//cudaDeviceSynchronize();

}

__global__
void _axpy_(const double alpha, const double* X, const double beta, double* Y, const int nr, const int nc){
	// map threads to 2D matrix
	int ix = threadIdx.x + blockIdx.x*blockDim.x;
	int iy = threadIdx.y + blockIdx.y*blockDim.y;
	int idx = ix + iy*nc;
	
	// for threads mapped to locations outside of matrix, return
	if(iy>=nr || ix>=nc)
		return;
	
	Y[idx] = alpha*X[idx] + beta*Y[idx];
}

void _dev_axpy_(const double alpha, const double* X, const double beta, double* Y, const int nr, const int nc, cudaStream_t* stream){
	int threadsPerBlock = 256;
	dim3 threads(1,1);
	threads.x = 16;
	threads.y = (threadsPerBlock+threads.x-1)/threads.x;

	dim3 blocks(1,1);
	blocks.x = (nc + threads.x-1)/threads.x;
	blocks.y = (nr + threads.y-1)/threads.y;

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	if(!stream)
		_axpy_<<<blocks,threads>>>(alpha,X,beta,Y,nr,nc);
	else
		_axpy_<<<blocks,threads,0,*stream>>>(alpha,X,beta,Y,nr,nc);

	#ifdef _check
	check_launch ("_axpy_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "AXPY call took: " << time << " seconds" << std::endl;
	#endif

	//cudaDeviceSynchronize();
}

/* -----------------------------------------------------------------------------
				Vectorized functions
-------------------------------------------------------------------------------*/
__global__
void _sigmoid_(const double* in, double* out, const int size){
	int tid = threadIdx.x + blockDim.x*(blockIdx.x+gridDim.x*blockIdx.y);
	if(tid>=size)
		return;
	out[tid] = 1./(1.+exp(-in[tid]));
}

void _dev_sigmoid_(double* out, const double* in, const int size, cudaStream_t* stream){
	dim3 threads(256,1);
	int x = ceil(sqrt((size + threads.x - 1)/threads.x));
	dim3 blocks(x,x);

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	if(!stream)
		_sigmoid_<<<blocks,threads>>>(in, out, size);
	else
		_sigmoid_<<<blocks,threads,0,*stream>>>(in, out, size);

	#ifdef _check
	check_launch ("_sigmoid_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "Sigmoid call took: " << time << " seconds" << std::endl;
	#endif

	//cudaDeviceSynchronize();
}

// TODO optimize as row-vec mult
__global__
void _softmax_(const double* in, double* out, const int nr, const int nc){
	int row = threadIdx.x + blockDim.x*blockIdx.x;
	if(row>=nr)
		return;
	// each threads handles one row
	double sum = 0;
	double max = in[row*nc];
	for(int i = 0; i<nc; i++)
		max = (in[row*nc+i]>max)?in[row*nc+i]:max;
	for(int i = 0; i<nc; i++)
		sum+= exp(in[row*nc+i]-max);
	for(int i = 0; i<nc; i++)
		out[row*nc+i] = exp(in[row*nc+i]-max)/sum;
}

void _dev_softmax_(double* out, const double* in, const int nr, const int nc, cudaStream_t* stream){
	int threads = 256;
	int blocks = (nr+threads-1)/threads;

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	if(!stream)
		_softmax_<<<blocks,threads>>>(in, out, nr, nc);
	else
		_softmax_<<<blocks,threads,0,*stream>>>(in, out, nr, nc);

	#ifdef _check
	check_launch ("_softmax_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "Softmax call took: " << time << " seconds" << std::endl;
	#endif

	//cudaDeviceSynchronize();
}

/* -----------------------------------------------------------------------------
				Special functions
-------------------------------------------------------------------------------*/
__global__
void _shortsum_(const double*in, const int size, double* out){
	*out = 0.;
	for(int i =0; i<size; i++)
		*out += in[i];
}


__global__
void _crossentropy_(const double* Yc, const double* Y, const int size, double* out){
	int tid = threadIdx.x;
	out[tid] = 0;
	while(tid<size){
		out[threadIdx.x] -=(Y[tid]==1)*log(Yc[tid]);
		tid += blockDim.x;
	}
}

double _dev_crossentropy_(const double* Yc, const double* Y, const int size, cudaStream_t* stream){

	int threads = 256;
	// allocate out
	double* out;
	checkCudaErrors(cudaMalloc((void**)&out,threads*sizeof(double)));
	double* val;
	checkCudaErrors(cudaMalloc((void**)&val,sizeof(double)));

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	if(!stream)
		_crossentropy_<<<1,threads>>>(Yc, Y, size, out);
	else
		_crossentropy_<<<1,threads,0,*stream>>>(Yc, Y, size, out);

	#ifdef _check
	check_launch ("_crossentropy_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "Crossentropy call took: " << time << " seconds" << std::endl;
	#endif

	#ifdef _prof_gpu
	event_pair timer2;
	start_timer (&timer2);
	#endif
	
	if(!stream)
		_shortsum_<<<1,1>>>(out, threads, val);
	else
		_shortsum_<<<1,1,0,*stream>>>(out, threads, val);
		
	#ifdef _check
	check_launch ("_shortsum_");
	#endif

	#ifdef _prof_gpu
	double time2 = stop_timer (&timer2);
	std::cout << "shortsum call took: " << time2 << " seconds" << std::endl;
	#endif

	//memcpy
	double ret = 0;
	checkCudaErrors(cudaMemcpy(&ret,val,sizeof(double),cudaMemcpyDeviceToHost));
	// dealloc out, val
	checkCudaErrors(cudaFree(out));
	checkCudaErrors(cudaFree(val));

	//cudaDeviceSynchronize();
	return ret;
}

__global__
void _square_(const double* Y, const int size, double* out){
	out[threadIdx.x] = 0;
	for(int tid = threadIdx.x; tid<size; tid=tid+blockDim.x)
		out[threadIdx.x] += Y[tid]*Y[tid];
}

double _dev_square_(const double* Y, const int size, cudaStream_t* stream){

	int threads = 64;
	// allocate out
	double* out;
	checkCudaErrors(cudaMalloc((void**)&out,threads*sizeof(double)));
	double* val;
	checkCudaErrors(cudaMalloc((void**)&val,sizeof(double)));

	#ifdef _prof_gpu
	event_pair timer;
	start_timer (&timer);
	#endif

	if(!stream)
		_square_<<<1,threads>>>(Y, size, out);
	else
		_square_<<<1,threads,0,*stream>>>(Y, size, out);

	#ifdef _check
	check_launch ("_square_");
	#endif

	#ifdef _prof_gpu
	double time = stop_timer (&timer);
	std::cout << "Square call took: " << time << " seconds" << std::endl;
	#endif

	#ifdef _prof_gpu
	event_pair timer2;
	start_timer (&timer2);
	#endif

	if(!stream)
		_shortsum_<<<1,1>>>(out, threads, val);
	else
		_shortsum_<<<1,1,0,*stream>>>(out, threads, val);

	#ifdef _check
	check_launch ("_shortsum_");
	#endif

	#ifdef _prof_gpu
	double time2 = stop_timer (&timer2);
	std::cout << "shortsum call took: " << time2 << " seconds" << std::endl;
	#endif

	//memcpy
	double ret = 0;
	checkCudaErrors(cudaMemcpy(&ret,val,sizeof(double),cudaMemcpyDeviceToHost));
	// dealloc out, val
	checkCudaErrors(cudaFree(out));
	checkCudaErrors(cudaFree(val));

	//cudaDeviceSynchronize();
	return ret;
}

/*-----------------------------------------------------------------------------
 *  Host functions
 *-----------------------------------------------------------------------------*/
void _host_gemm_(double* Z, const double* A, const double* W, const double* b, const double alpha, const double beta, const int Anr, const int Anc, const int Wnr, const int Wnc, const int Bnr, const int Bnc, const int type){}


void _host_axpby_(double* Z, const double alpha, const double* X, const double beta, const double* Y, const int nr, const int nc){ }

void _host_axpy_(const double alpha, const double* X, const double beta, double* Y, const int nr, const int nc){ }

void _host_reduce_(const double* mat, const int nr, const int nc, double* rowvec){ }

void _host_repmat_(double* mat, const int nr, const int nc, const double* rowvec){ }

void _host_sigmoid_(double* out, const double* in, const int size){ }

void _host_softmax_(double* out, const double* in, const int nr, const int nc){ }

double _host_square_(const double* Y, const int size){ return 0; }
double _host_crossentropy_(const double* Yc, const double* Y, const int size){ return 0; }

