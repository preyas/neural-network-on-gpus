#ifndef GPU_FUNC_H_
#define GPU_FUNC_H_

#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>
#include <stdio.h>
#include "defs.h"

struct event_pair
{
	cudaEvent_t start;
	cudaEvent_t end;
};

inline void check_launch(const char * kernel_name)
{
	cudaThreadSynchronize();
	cudaError_t err = cudaGetLastError();
	if(err != cudaSuccess)
	{
		std::cerr << "error in " << kernel_name << " kernel" << std::endl;
		std::cerr << "error was: " << cudaGetErrorString(err) << std::endl;
		exit(1);
	}
}

inline void start_timer(event_pair * p)
{
	cudaEventCreate(&p->start);
	cudaEventCreate(&p->end);
	cudaEventRecord(p->start, 0);
}


inline double stop_timer(event_pair * p)
{
	cudaEventRecord(p->end, 0);
	cudaEventSynchronize(p->end);

	float elapsed_time;
	cudaEventElapsedTime(&elapsed_time, p->start, p->end);
	cudaEventDestroy(p->start);
	cudaEventDestroy(p->end);
	return elapsed_time;
}

/*-----------------------------------------------------------------------------
 *  GPU function headers
 *-----------------------------------------------------------------------------*/
 // Jun 1
void DoGEMM_Jun1(double *Z, double* A, double* W, double* b, double alpha, double beta, int Bnr, int Bnc, int Anc, int type);

// Jun 10
void _dev_gemm_(double *, const double* , const double* , const double* , const double ,const double , const int , const int , const int , const int, const int, const int ,const int, cudaStream_t* stream=0);
void _dev_axpby_(double*, const double, const double*, const double, const double*, const int, const int, cudaStream_t* stream=0);
void _dev_axpy_(const double, const double*, const double, double*, const int, const int, cudaStream_t* stream=0);
void _dev_reduce_(const double*, const int, const int, double*, cudaStream_t* stream=0);
void _dev_repmat_(double*, const int, const int, const double*, cudaStream_t* stream=0);
void _dev_sigmoid_(double*, const double*, const int, cudaStream_t* stream=0);
void _dev_softmax_(double*, const double*, const int, const int, cudaStream_t* stream=0);
double _dev_crossentropy_(const double*, const double*, const int, cudaStream_t* stream=0);
double _dev_square_(const double*, const int, cudaStream_t* stream=0);

// dangerous
void Copy(double*&, const double* , const char, const char, int, char sync='s', cudaStream_t* stream=0);

// memory and access management
void AllocLinDevMem(double*&, const int);
void DeallocLinDevMem(double*&);
void AllocLinHostMem(double*&, const int, bool);
void DeallocLinHostMem(double*&, bool);

// for easy functionality
void FillLinDevMem(double*, const int, const double);
void AllocFillLinDevMem(double*&, const int, const double);

// debug functionality
double Getval(double*&, const int);
void Setval(double*&, const int, const double);

/*-----------------------------------------------------------------------------
 *  Host functon headers
 *-----------------------------------------------------------------------------*/
void _host_gemm_(double *, const double* , const double* , const double* , const double ,const double , const int , const int , const int , const int, const int, const int ,const int);
void _host_axpby_(double*, const double, const double*, const double, const double*, const int, const int);
void _host_axpy_(const double, const double*, const double, double*, const int, const int);
void _host_reduce_(const double*, const int, const int, double*);
void _host_repmat_(double*, const int, const int, const double*);
void _host_sigmoid_(double*, const double*, const int);
void _host_softmax_(double*, const double*, const int, const int);
double _host_crossentropy_(const double*, const double*, const int);
double _host_square_(const double*, const int);

#endif
