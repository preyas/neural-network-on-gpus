#include <iostream>
#include <cassert>
#include <mpi.h>
#include <armadillo>
#include <cuda_runtime.h>
#include <helper_cuda.h>
#include <helper_functions.h>
#include <unistd.h>

#include "utils/mnist.h"
#include "utils/two_layer_net.h"
#include "utils/test_utils.h"
#include "utils/common.h"
#include "defs.h"
#include "gpu_func.h"
#include "simple_mat.h"

#define FILE_TRAIN_IMAGES "data/train-images-idx3-ubyte"
#define FILE_TRAIN_LABELS "data/train-labels-idx1-ubyte"
#define FILE_TEST_IMAGES "data/t10k-images-idx3-ubyte"
#define FILE_TEST_OUTPUT "test_output.out"
#define NUM_TRAIN 60000
#define IMAGE_SIZE 784  // 28 x 28
#define NUM_CLASSES 10
#define NUM_TEST 10000

#define MPI_SAFE_CALL( call ) do {                               \
    int err = call;                                              \
    if (err != MPI_SUCCESS) {                                    \
        fprintf(stderr, "MPI error %d in file '%s' at line %i",  \
               err, __FILE__, __LINE__);                         \
        exit(1);                                                 \
    } } while(0)

#ifdef _main_1
int main (int argc, char *argv[]) {
	// initialize
	int num_procs = 0, rank = 0;
	MPI_Init (&argc, &argv);
	MPI_SAFE_CALL (MPI_Comm_size (MPI_COMM_WORLD, &num_procs));
	MPI_SAFE_CALL (MPI_Comm_rank (MPI_COMM_WORLD, &rank));

	int nDevices;
	cudaGetDeviceCount(&nDevices);
	checkCudaErrors(cudaSetDevice(rank));

	#ifdef _debug
	if(rank == 0){
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp,0); 
	std::cout<<"max mem: "<<deviceProp.totalGlobalMem/1048576.0f<<" MB"<<std::endl;
	std::cout<<"supports unified mem access: "<<deviceProp.unifiedAddressing<<std::endl;
	std::cout<<"max threads per multiprocessor: "<<deviceProp.maxThreadsPerMultiProcessor<<std::endl;
	}
	#endif

	// reads in options
	std::vector<int> H(3);
	double reg = 1e-4;
	double learning_rate = 0.05;
	int num_epochs = 2;
	int batch_size = 10000;
	int num_neuron = 1000;
	int run_seq = 0;
	int run_jun1 = 0;
	int run_jun10 = 0;

	int option = 0;
	while ((option = getopt(argc, argv, "n:r:l:e:b:s:p:o")) != -1) {
		switch (option) {
			case 'n': num_neuron = atoi(optarg); break;
			case 'r': reg = atof(optarg); break;
			case 'l': learning_rate = atof(optarg); break;
			case 'e': num_epochs = atoi(optarg); break;
			case 'b': batch_size = atoi(optarg); break;
			case 's': run_seq = atoi(optarg); break;
			case 'p': run_jun1 = atoi(optarg); break;
			case 'o': run_jun10 = 1; break;
		}
	}

	H[0] = IMAGE_SIZE;
	H[1] = num_neuron;
	H[2] = NUM_CLASSES;

	arma::mat x_train, y_train, label_train, x_dev, y_dev, label_dev, x_test;
	TwoLayerNet nn (H);

	if (rank == 0) {
		double startread = MPI_Wtime();
		std::cout << "num_neuron=" << num_neuron << ", reg=" << reg << ", learning_rate=" << learning_rate
			<< ", num_epochs=" << num_epochs << ", batch_size=" << batch_size << std::endl;
		// Read MNIST images into Armadillo mat vector
		arma::mat x (NUM_TRAIN, IMAGE_SIZE);
		// label_train contains the prediction for each 
		arma::colvec label = arma::zeros<arma::colvec>(NUM_TRAIN);
		// y_train is the matrix of one-hot label vectors where only y[c] = 1, 
		// where c is the right class.
		arma::mat y = arma::zeros<arma::mat>(NUM_TRAIN, NUM_CLASSES);

		std::cout << "Loading training data..." << std::endl;
		read_mnist (FILE_TRAIN_IMAGES, x);
		read_mnist_label (FILE_TRAIN_LABELS, label);
		label_to_y (label, NUM_CLASSES, y);

		/* Print stats of training data */
		std::cout << "Training data stats..." << std::endl;
		std::cout << "Size of x_train, N =  " << x.n_rows << std::endl;
		std::cout << "Size of label_train = " << label.size() << std::endl;

		assert (x.n_rows == NUM_TRAIN && x.n_cols == IMAGE_SIZE);
		assert (label.size() == NUM_TRAIN);

		/* Split into train set and dev set, you should use train set to train your
		neural network and dev set to evaluate its precision */
		int dev_size = (int) (0.1 * NUM_TRAIN);
		x_train = x.rows (0, NUM_TRAIN-dev_size-1);
		y_train = y.rows (0, NUM_TRAIN-dev_size-1);
		label_train = label.rows (0, NUM_TRAIN-dev_size-1);

		x_dev = x.rows (NUM_TRAIN-dev_size, NUM_TRAIN - 1);
		y_dev = y.rows (NUM_TRAIN-dev_size, NUM_TRAIN - 1);
		label_dev = label.rows (NUM_TRAIN-dev_size, NUM_TRAIN - 1);

		/* Load the test data, we will compare the prediction of your trained neural 
		network with test data label to evalute its precision */
		x_test = arma::zeros (NUM_TEST, IMAGE_SIZE);
		read_mnist (FILE_TEST_IMAGES, x_test);
		double endread = MPI_Wtime();
		std::cout<<" Reading and preparing data set took "<<endread-startread<<" seconds "<<std::endl;
	}

	if ((rank == 0) && (run_seq)) {
		TwoLayerNet seq_nn (H);
		std::cout << "Start Sequential Training" << std::endl;
		double start = MPI_Wtime();
		train (seq_nn, x_train, y_train, learning_rate, reg, num_epochs, batch_size, false, 10000);
		double end = MPI_Wtime();
		std::cout << "Time for Sequential Training: " << end - start << " seconds" << std::endl;
		start = MPI_Wtime();
		arma::vec label_dev_pred;
		predict (seq_nn, x_dev, label_dev_pred);
		double prec = precision (label_dev_pred, label_dev);
		end = MPI_Wtime();
		std::cout << "Precision on dev set for sequential training = " << prec << std::endl;
		std::cout << "Time for prediction on dev = " << end-start <<" seconds"<< std::endl;
		std::cout<<std::endl;
		std::cout<<"####################################################################################"<<std::endl;
		std::cout<<"####################################################################################"<<std::endl;
		std::cout<<std::endl;
	}
   
if(run_jun1){
  	/* ---- Parallel Training ---- */
	// Train the Neural Network in Parallel
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == 0) 
		std::cout << "Start Parallel Training With Jun1 code" << std::endl;
	double start = MPI_Wtime();
   	
	par_train_Jun1_withMPI (nn, x_train, y_train, learning_rate, reg, num_epochs, batch_size, false, 10000);
    
	double end = MPI_Wtime();
	if (rank == 0)
		std::cout << "Time for Parallel Training: " << end - start << " seconds" << std::endl;

	/* Make sure after training process, rank 0's neural network is up to date */
	if (rank == 0) {
		start = MPI_Wtime();
		arma::vec label_dev_pred;
		parpredict_Jun1 (nn, x_dev, label_dev_pred);
		double prec = precision (label_dev_pred, label_dev);
		std::cout << "Precision on dev set for parallel training = " << prec << std::endl;
		arma::vec label_test_pred;
		parpredict_Jun1 (nn, x_test, label_test_pred);
		end = MPI_Wtime();
		save_label (FILE_TEST_OUTPUT, label_test_pred);
		std::cout << "Time for prediction on dev= " << end-start <<" seconds" << std::endl;
		std::cout<<std::endl;
		std::cout<<"####################################################################################"<<std::endl;
		std::cout<<"####################################################################################"<<std::endl;
		std::cout<<std::endl;
	}
}

if(run_jun10){
	double s1 = MPI_Wtime();
	/* Prime the data set*/
	x_train = x_train.t();
	y_train = y_train.t();
	smat X_tr,y_tr;
	if(rank == 0){
		X_tr.copy(x_train.n_cols,x_train.n_rows,x_train.memptr(),'h','h');
		y_tr.copy(y_train.n_cols,y_train.n_rows,y_train.memptr(),'h','h');
	}
	x_train = x_train.t();
	y_train = y_train.t();
	double s2 = MPI_Wtime();
	std::cout<<"time to prime data set "<<(s2-s1)<<" seconds\n";

  	/* ---- Parallel Training ---- */
	MPI_Barrier(MPI_COMM_WORLD);
	if (rank == 0) 
		std::cout << "Start Parallel Training" << std::endl;
	double start2 = MPI_Wtime();

	STwoLayerNet snn (H,'d');
	par_train_Jun10_withMPI (snn, X_tr, y_tr, learning_rate, reg, num_epochs, batch_size, false, 10000);
    
	double end2 = MPI_Wtime();
	if (rank == 0)
		std::cout << "Time for Parallel Training: " << end2 - start2 << " seconds" << std::endl;

	// predict on dev and test sets
	if (rank == 0) {
		start2 = MPI_Wtime();		
		// copy network coeffs to host
		STwoLayerNet hnn(snn,'h');
		for(int i =0; i<hnn.W.size(); i++){
			arma::mat hW(hnn.W[i].memptr(),hnn.W[i].n_cols,hnn.W[i].n_rows, true, true);
			nn.W[i] = hW.t();
			arma::rowvec hb(hnn.b[i].memptr(),hnn.b[i].n_cols, true, true);
			nn.b[i] = hb;
		}
		arma::vec label_dev_pred;
		parpredict_Jun10 (nn, x_dev, label_dev_pred);
		double prec = precision (label_dev_pred, label_dev);
		std::cout << "Precision on dev set for parallel training = " << prec << std::endl;
		arma::vec label_test_pred;
		parpredict_Jun1 (nn, x_test, label_test_pred);
		end2 = MPI_Wtime();		
		save_label (FILE_TEST_OUTPUT, label_test_pred);
		std::cout << "Time for prediction on dev = " << end2-start2 <<" seconds" << std::endl;
		std::cout<<std::endl;
		std::cout<<"####################################################################################"<<std::endl;
		std::cout<<"####################################################################################"<<std::endl;
		std::cout<<std::endl;
	}
}

	MPI_Finalize ();
	return 0;
}

#endif


#ifdef _test_gemm

int main (int argc, char *argv[]) {
	// initialize
	int num_procs = 0, rank = 0;
	MPI_Init (&argc, &argv);
	MPI_SAFE_CALL (MPI_Comm_size (MPI_COMM_WORLD, &num_procs));
	MPI_SAFE_CALL (MPI_Comm_rank (MPI_COMM_WORLD, &rank));

	int a = 10;
	int b = 200;
	int c = 15;

	// create large matrices to do serial and CUDA GEMM
	// A
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(0));
	arma::mat A = arma::randn(a,b);
	A = A.t();
	smat sA;
	sA.copy(A.n_cols,A.n_rows,A.memptr(),'d','h');
	A = A.t();

	// B
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(0));
	arma::mat B = arma::randn(a,c);
	B = B.t();
	smat sB;
	sB.copy(B.n_cols,B.n_rows,B.memptr(),'d','h');
	B = B.t();
	
	// C	
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(0));
	arma::mat C = arma::randn(b,c);
	C = C.t();
	smat sC;
	sC.copy(C.n_cols,C.n_rows,C.memptr(),'d','h');
	C = C.t();

	// Z
	arma::mat Z = A.t()*B + C;
	smat sZ(b,c,'d');
	smat::gemm(sZ, 1., sA, sB, 1., sC, 1);
/*
	std::cout<<std::setprecision(6);
	std::cout<<"Z = "<<std::endl;
	Z.raw_print();
	std::cout<<std::endl;
	std::cout<<"Z = "<<std::endl;
	sZ.print();
*/
	std::cout<<std::setprecision(16);
	Z.raw_print();
	std::cout<<std::endl;
	sZ.print();
	MPI_Finalize ();
	return 0;
}

#endif


#ifdef _test_mat
int main (int argc, char *argv[]) {
	// initialize
	int num_procs = 0, rank = 0;
	MPI_Init (&argc, &argv);
	MPI_SAFE_CALL (MPI_Comm_size (MPI_COMM_WORLD, &num_procs));
	MPI_SAFE_CALL (MPI_Comm_rank (MPI_COMM_WORLD, &rank));

	// test gemm
	// A
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(0));
	arma::mat A = arma::randn(5,8);
	A = A.t();
	smat sA;
	sA.copy(A.n_cols,A.n_rows,A.memptr(),'d','h');
	A = A.t();

	// B
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(0));
	arma::mat B = arma::randn(9,8);
	B = B.t();
	smat sB;
	sB.copy(B.n_cols,B.n_rows,B.memptr(),'d','h');
	B = B.t();
	
	// C	
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(0));
	arma::mat C = arma::randn(5,9);
	C = C.t();
	smat sC;
	sC.copy(C.n_cols,C.n_rows,C.memptr(),'d','h');
	C = C.t();

	// Z
	arma::mat Z = A*B.t() + C;
	smat sZ(5,9,'d');
	smat::gemm(sZ, 1., sA, sB, 1., sC, 0);

	// test sigmoid
	arma::mat S;
	S.set_size(Z.n_rows,Z.n_cols);
	S = 1/(1 + arma::exp(-Z));
	smat sS(5,9,'d');
	smat::sig(sS,sZ);

	//reduce
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(0));
	arma::mat R = arma::randn(5,20);
	arma::mat r = sum(R,0);
	R = R.t();
	smat sR;
	sR.copy(R.n_cols,R.n_rows,R.memptr(),'d','h');
	R = R.t();
	smat sr(1,20,'d');
	smat::reduce(sr,sR);

	//softmax
	arma::mat F;
	softmax(Z,F);
	smat sF(5,9,'d');
	smat::sf(sF,sZ);

	//norms
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(10));
	arma::mat N = arma::randn(5,20);
	N = N.t();
	smat sN;
	sN.copy(N.n_cols,N.n_rows,N.memptr(),'d','h');
	N = N.t();
	double no = arma::accu (arma::square( N) );

	// CE
	arma::arma_rng::set_seed(arma::arma_rng::seed_type(10));
	arma::mat E = arma::randn(5,20);
	E = E.t();
	smat sE;
	sE.copy(E.n_cols,E.n_rows,E.memptr(),'d','h');
	E = E.t();

	arma::arma_rng::set_seed(arma::arma_rng::seed_type(10));
	arma::mat SP = arma::randn(5,20);
	for(int i =0; i<SP.n_rows; i++){
		for(int j = 0; j<SP.n_cols; j++){
			if(SP(i,j)>0)
				SP(i,j) = 1;
			else
				SP(i,j) = 0;
		}
	}
	SP = SP.t();
	smat sSP;
	sSP.copy(SP.n_cols,SP.n_rows,SP.memptr(),'d','h');
	SP = SP.t();
	double ce = - arma::accu( arma::log( E.elem (arma::find (SP==1))));
	double ce2 = smat::CE(sE,sSP);
	std::cout<<ce<<" "<<ce2<<std::endl;

	MPI_Finalize ();
	return 0;
}

#endif
