/*
 * =====================================================================================
 *
 *       Filename:  simple_mat.cpp
 *
 *    Description:  matrix class member functions 
 *
 *        Version:  1.0
 *        Created:  05/31/2015 03:52:05 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Preyas Shah (), 
 *   Organization:  Stanford University
 *
 * =====================================================================================
 */

#include "simple_mat.h"

/*-----------------------------------------------------------------------------
 *  constructors
 *-----------------------------------------------------------------------------*/
//default
smat::smat(){
	n_rows = 1;
	n_cols = 1;
	isAllocated = false;
	isPinned=false;
	IsOn = 0;
}

// copy from pre-allocated mem
// can alloc async with dst pinned
// otherwise sync
void smat::copy(const int nr, const int nc, const double* mem, const char dst, const char src, bool dstp, bool srcp, char sync, cudaStream_t* stream){
	#ifdef _check
	assert(src=='h' or src=='d');
	assert(dst=='h' or dst=='d');
	assert(nr>0);
	assert(nc>0);
	assert(sync=='a' or sync=='s');
	#endif
	if(isAllocated && nc==n_cols && nr==n_rows && IsOn==dst){
		if(IsOn == 'h' && isPinned==dstp){
			Copy(mat,mem,src,dst,n_rows*n_cols,sync,stream);
			isPinned=dstp;
		}
		if(IsOn =='h' && isPinned!=dstp){
			alloc(nr,nc,dst,dstp);
			Copy(mat,mem,src,dst,n_rows*n_cols,sync,stream);
		}
		if(IsOn == 'd'){
			Copy(mat,mem,src,dst,n_rows*n_cols,sync,stream);
		}
	}
	else{
		alloc(nr,nc,dst,dstp);
		Copy(mat,mem,src,dst,n_rows*n_cols,sync,stream);
		IsOn = dst;
		isAllocated = true;
	}
}

void smat::size(const int nr, const int nc, const char hd, bool pinned){

	#ifdef _check
	assert(hd=='h' or hd=='d');
	assert(nr>0);
	assert(nc>0);
	#endif
	
	// smart alloc
	if(isAllocated && IsOn==hd && nr==n_rows && nc==n_cols){
		if(IsOn=='h' && isPinned==pinned)
			return;
		if(IsOn=='d')
			return;
	}
	reset();
	n_rows = nr;
	n_cols = nc;
	isAllocated = true;
	IsOn = hd;
	if(hd=='h')
		isPinned = pinned;
	else
		isPinned = false;
	if(hd=='h'){
		AllocLinHostMem(mat,n_rows*n_cols,isPinned);
		for(int i = 0; i<n_rows*n_cols; i++)
			mat[i] = 0;
	}	
	if(hd=='d'){
		AllocFillLinDevMem(mat,n_rows*n_cols,0.);
	}
}

void smat::alloc(const int nr, const int nc, const char hd, bool pinned){

	#ifdef _check
	assert(hd=='h' or hd=='d');
	assert(nr>0);
	assert(nc>0);
	#endif
	
	// smart alloc
	if(isAllocated && IsOn==hd && nr==n_rows && nc==n_cols){
		if(IsOn=='h' && isPinned==pinned)
			return;
		if(IsOn=='d')
			return;
	}
	reset();
	n_rows = nr;
	n_cols = nc;
	isAllocated = true;
	IsOn = hd;
	if(hd=='h')
		isPinned = pinned;
	else
		isPinned = false;
	if(hd=='h')
		AllocLinHostMem(mat,n_rows*n_cols,pinned);
	else
		AllocLinDevMem(mat,n_rows*n_cols);
}

// device or host
smat::smat(const int nr, const int nc, const char hd, bool pinned){
	#ifdef _check
	assert(hd=='h' or hd=='d');
	assert(nr>0);
	assert(nc>0);
	#endif
	n_rows = nr;
	n_cols = nc;
	IsOn = hd;
	if(hd=='h')
		isPinned=pinned;
	else
		isPinned=false;
	if(hd=='h'){
		AllocLinHostMem(mat,n_rows*n_cols,isPinned);
		for(int i = 0; i<n_rows*n_cols; i++)
			mat[i] = 0;
	}	
	if(hd=='d'){
		AllocFillLinDevMem(mat,n_rows*n_cols,0.);
	}
	isAllocated = true;
}

//deep, intelligent copy only
smat::smat(const smat& source){
	#ifdef _check
	assert(source.n_rows>0);
	assert(source.n_cols>0);
	#endif
	n_rows = source.n_rows;
	n_cols = source.n_cols;
	IsOn = source.IsOn;
	isAllocated = source.isAllocated;
	if(IsOn=='h')
		AllocLinHostMem(mat,n_rows*n_cols,false);
	else
		AllocLinDevMem(mat,n_rows*n_cols);
	isPinned = false;
	Copy(mat, source.mat, IsOn, IsOn, n_rows*n_cols,'s');
}

/*-----------------------------------------------------------------------------
 *  operators/ assignment
 *-----------------------------------------------------------------------------*/
// deep copy assignment, always synchronous
smat& smat::operator=(const smat& source){
	#ifdef _check
	assert(source.n_rows>0);
	assert(source.n_cols>0);
	#endif
	if(this!=&source){
		reset();
		n_rows = source.n_rows;
		n_cols = source.n_cols;
		IsOn = source.IsOn;
		if(IsOn=='h')
			AllocLinHostMem(mat,n_rows*n_cols,false);
		else
			AllocLinDevMem(mat,n_rows*n_cols);
		Copy(mat, source.mat, IsOn, IsOn, n_rows*n_cols,'s');
	}
	isPinned = false;
	isAllocated = source.isAllocated;
	return *this;
}

/*-----------------------------------------------------------------------------
 *  accessors, mutators
 *-----------------------------------------------------------------------------*/

// dangerous, coz it could be pointing to the device or the host.
double* const smat::memptr(){
	#ifdef _check
	assert(isAllocated);
	#endif
	double* ret = mat;
	return ret;
}

// for debug
double smat::operator()(const int r, const int c){
	#ifdef _check
	assert(r<n_rows);
	assert(r>=0);
	assert(c>=0);
	assert(c<n_cols);
	assert(isAllocated);
	#endif

	double val = 0;
	if(IsOn=='h')
		val = mat[r*n_cols + c];
	else
		val = Getval(mat,r*n_cols+c);
}

double smat::getAt(const int r, const int c){
	#ifdef _check
	assert(r<n_rows);
	assert(r>=0);
	assert(c>=0);
	assert(c<n_cols);
	assert(isAllocated);
	#endif

	double val = 0;
	if(IsOn=='h')
		val = mat[r*n_cols + c];
	else
		val = Getval(mat,r*n_cols+c);
	return val;
}

void smat::setAt(const int r, const int c, const double val){
	#ifdef _check
	assert(r<n_rows);
	assert(r>=0);
	assert(c>=0);
	assert(c<n_cols);
	assert(isAllocated);
	#endif

	if(IsOn=='h')
		mat[r*n_cols + c] = val;
	else
		Setval(mat,r*n_cols+c,val);
}

// for debug
void smat::print(){
	#ifdef _check
	assert(n_rows>0);
	assert(n_cols>0);
	#endif
	for(int i = 0; i<n_rows; i++){
		for(int j = 0; j<n_cols; j++)
			std::cout<<getAt(i,j)<<" ";
		std::cout<<std::endl;
	}
	std::cout<<std::endl;
}

/*-----------------------------------------------------------------------------
 *  destructor
 *-----------------------------------------------------------------------------*/
void smat::reset(){
	if(isAllocated){
		if(IsOn=='h')
			DeallocLinHostMem(mat,isPinned);
		if(IsOn=='d')
			DeallocLinDevMem(mat);
		isAllocated = false;
	}
	IsOn = 0;
	isPinned = false;
	isAllocated = false;
	n_cols = 1;
	n_rows = 1;
}

smat::~smat(){
	if(IsOn=='h')
		DeallocLinHostMem(mat,isPinned);
	if(IsOn=='d')
		DeallocLinDevMem(mat);
	isPinned = false;
	isAllocated = false;
}

/*-----------------------------------------------------------------------------
 *  class objects
 *-----------------------------------------------------------------------------*/
smat smat::zeros(const int nr, const int nc, const char hd){
	smat ret(nr,nc,hd);
	if(hd=='h'){
		for(int i =0; i<ret.n_rows*ret.n_cols; i++)
			ret.memptr()[i] = 0.;
	}
	else{
		FillLinDevMem(ret.memptr(),ret.n_rows*ret.n_cols,1.);
	}
	return ret;
}

smat smat::ones(const int nr, const int nc, const char hd){
	smat ret(nr,nc,hd);
	if(hd=='h'){
		for(int i =0; i<ret.n_rows*ret.n_cols; i++)
			ret.memptr()[i] = 1.;
	}
	else{
		FillLinDevMem(ret.memptr(),ret.n_rows*ret.n_cols,1.);
	}
	return ret;
}

/*-----------------------------------------------------------------------------
 *  Wrappers for standard and specialized ops on matrix objects
 *-----------------------------------------------------------------------------*/

void smat::gemm(smat& Z, const double alpha, smat& A, smat&B, const double beta, smat& C, const int type, cudaStream_t* stream){

	#ifdef _check
	assert(Z.isAllocated && A.isAllocated && B.isAllocated && C.isAllocated);
	assert(Z.n_rows==C.n_rows);
	assert(Z.n_cols==C.n_cols);
	assert(type<=2 && type>=0);
	assert(Z.IsOn==A.IsOn && A.IsOn==B.IsOn && B.IsOn==C.IsOn);
	if(type==0){
		assert(Z.n_rows==A.n_rows);
		assert(Z.n_cols==B.n_rows);
		assert(A.n_cols==B.n_cols);
	}
	if(type==1){
		assert(Z.n_rows==A.n_cols);
		assert(Z.n_cols==B.n_cols);
		assert(B.n_rows==A.n_rows);
	}
	if(type==2){
		assert(Z.n_rows==A.n_rows);
		assert(Z.n_cols==B.n_cols);
		assert(A.n_cols==B.n_rows);
	}
	#endif
	if(Z.IsOn=='d')
		_dev_gemm_(Z.memptr(),A.memptr(),B.memptr(),C.memptr(),alpha,beta,A.n_rows,A.n_cols,B.n_rows,B.n_cols,C.n_rows,C.n_cols,type,stream);
	if(Z.IsOn=='h')
		_host_gemm_(Z.memptr(),A.memptr(),B.memptr(),C.memptr(),alpha,beta,A.n_rows,A.n_cols,B.n_rows,B.n_cols,C.n_rows,C.n_cols,type);
		
}

// Z = a*X+b*Y
void smat::axpby(smat& Z, const double a, smat& X, const double b, smat& Y, cudaStream_t* stream){

	#ifdef _check
	assert(Z.isAllocated && X.isAllocated && Y.isAllocated);
	assert(Z.n_rows==X.n_rows);
	assert(Y.n_rows==X.n_rows);
	assert(Z.n_cols==X.n_cols);
	assert(Y.n_cols==X.n_cols);
	assert(Z.IsOn==X.IsOn && X.IsOn==Y.IsOn);
	#endif
	if(Z.IsOn=='d')
		_dev_axpby_(Z.memptr(),a,X.memptr(),b,Y.memptr(),X.n_rows,X.n_cols,stream);
	if(Z.IsOn=='h')
		_host_axpby_(Z.memptr(),a,X.memptr(),b,Y.memptr(),X.n_rows,X.n_cols);
}

// axpy
void smat::axpy(const double a, smat& X, const double b, smat& Y, cudaStream_t* stream){

	#ifdef _check
	assert(X.isAllocated && Y.isAllocated);
	assert(Y.n_rows==X.n_rows);
	assert(Y.n_cols==X.n_cols);
	assert(X.IsOn==Y.IsOn);
	#endif
	if(X.IsOn=='d')
		_dev_axpy_(a,X.memptr(),b,Y.memptr(),X.n_rows,X.n_cols,stream);
	if(X.IsOn=='h')
		_host_axpy_(a,X.memptr(),b,Y.memptr(),X.n_rows,X.n_cols);
}

void smat::reduce(smat& Z, smat& X, cudaStream_t* stream){

	#ifdef _check
	assert(Z.isAllocated && X.isAllocated);
	assert(Z.n_rows==1);
	assert(X.n_cols == Z.n_cols);
	assert(Z.IsOn==X.IsOn);
	#endif
	if(Z.IsOn=='d')
		_dev_reduce_(X.memptr(), X.n_rows, X.n_cols, Z.memptr(),stream);
	if(Z.IsOn=='h')
		_host_reduce_(X.memptr(), X.n_rows, X.n_cols, Z.memptr());

}

void smat::repmat(smat& rvec, const int nr, cudaStream_t* stream){

	#ifdef _check
	assert(nr>0 && rvec.n_cols>0);
	assert(rvec.n_rows==1);
	assert(n_rows>0 && n_cols>0);
	#endif
	int realloc = 1;
	if(IsOn==rvec.IsOn && isAllocated && n_rows==nr && n_cols==rvec.n_cols)
		realloc = 0;
	if(realloc==1){
		reset();
		n_rows=nr;
		n_cols = rvec.n_cols;
		isAllocated = 1;
		IsOn = rvec.IsOn;
	}
	if(IsOn=='d'){
		if(realloc==1)
			AllocLinDevMem(mat,n_rows*n_cols);
		_dev_repmat_(mat,nr,rvec.n_cols,rvec.memptr(),stream);
	}
	if(IsOn=='h'){
		if(realloc==1)
			AllocLinHostMem(mat,n_rows*n_cols,false);
		_host_repmat_(mat,nr,rvec.n_cols,rvec.memptr());
	}
	isPinned = false;
}

void smat::sig(smat& out, smat& in, cudaStream_t* stream){

	#ifdef _check
	assert(out.n_rows==in.n_rows);
	assert(out.n_cols==in.n_cols);
	assert(out.IsOn==in.IsOn);
	#endif
	if(out.IsOn=='d')
		_dev_sigmoid_(out.memptr(), in.memptr(), in.n_rows*in.n_cols, stream);
	if(out.IsOn=='h')
		_host_sigmoid_(out.memptr(), in.memptr(), in.n_rows*in.n_cols);
}

void smat::sf(smat& out, smat& in, cudaStream_t* stream){

	#ifdef _check
	assert(out.n_rows==in.n_rows);
	assert(out.n_cols==in.n_cols);
	assert(out.IsOn==in.IsOn);
	#endif
	if(out.IsOn=='d')
		_dev_softmax_(out.memptr(), in.memptr(), in.n_rows, in.n_cols, stream);
	if(out.IsOn=='h')
		_host_softmax_(out.memptr(), in.memptr(), in.n_rows, in.n_cols);

}

double smat::CE(smat& Yc, smat& Y, cudaStream_t* stream){

	#ifdef _check
	assert(Yc.n_cols==Y.n_cols);
	assert(Yc.n_rows==Y.n_rows);
	assert(Yc.isAllocated && Y.isAllocated);
	assert(Yc.IsOn==Y.IsOn);
	#endif
	if(Yc.IsOn=='d')
		return _dev_crossentropy_(Yc.memptr(),Y.memptr(),Y.n_cols*Y.n_rows, stream);
	else
		return _host_crossentropy_(Yc.memptr(),Y.memptr(),Y.n_cols*Y.n_rows);
}

double smat::norm(cudaStream_t* stream){

	#ifdef _check
	assert(isAllocated);
	#endif
	if(IsOn=='d')
		return _dev_square_(mat,n_cols*n_rows, stream);
	else
		return _host_square_(mat,n_cols*n_rows);
}
