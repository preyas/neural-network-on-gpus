/*
 * =====================================================================================
 *
 *       Filename:  simple_mat.h
 *
 *    Description:  matrix class to replace armadillo matrices. Allows both row and column-wise representation. Also allows transpose using a cuda kernel.
 *
 *        Version:  1.0
 *        Created:  05/31/2015 03:50:21 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Preyas Shah (), 
 *   Organization:  Stanford University
 *
 * =====================================================================================
 */

#ifndef GPUMAT_H
#define GPUMAT_H

#include <iostream>
#include <cassert>
#include <vector>
#include <stdio.h>
#include "gpu_func.h"
#include <cuda_runtime.h>

class smat{

private:
	//memory properties
	double* mat;

	char IsOn;
	bool isAllocated;
	bool isPinned = false;

public:
	int n_rows;
	int n_cols;

	// accessors
	double* const memptr();
	double operator()(const int, const int);
	double getAt(const int, const int);
	void setAt(const int, const int, const double);
	void print();

	// constructors, initializers, assignment
	smat();
	void copy(const int, const int, const double*, const char, const char, bool dstp = false, bool srcp=false, char sync='s', cudaStream_t* stream=0);
	smat(const int , const int , const char, bool pinned=false);
	void size(const int, const int, const char, bool pinned=false);
	void alloc(const int, const int, const char, bool pinned=false);
	smat(const smat&);

	static smat ones(const int, const int, const char);
	static smat zeros(const int, const int, const char);
	smat& operator=(const smat&);

	void repmat(smat&, const int, cudaStream_t* stream=0);

	// destructor
	void reset();
	~smat();

	// useful wrappers
	static void gemm(smat&, const double, smat&, smat&, const double, smat&, const int, cudaStream_t* stream=0);
	static void axpby(smat&, const double, smat&, const double, smat&, cudaStream_t* stream=0);
	static void axpy(const double, smat&, const double, smat&, cudaStream_t* stream=0);
	static void reduce(smat&, smat&, cudaStream_t* stream=0);
	static void sf(smat&, smat&, cudaStream_t* stream=0);
	static void sig(smat&, smat&, cudaStream_t* stream=0);
	double norm(cudaStream_t* stream=0);
	static double CE(smat&, smat&, cudaStream_t* stream=0);
	
};

struct sgrads{
	std::vector<smat> dW;
	std::vector<smat> db;
	// intermediate calcs
	std::vector<smat> dz;
};

struct scache{
	smat X;
	std::vector<smat> z;
	std::vector<smat> a;
	smat yc;
	smat y;
	// intermediate calcs
	std::vector<smat> B;
};


#endif
