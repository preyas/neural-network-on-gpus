#include "two_layer_net.h"

#include <armadillo>
#include "common.h"
#include "../gpu_func.h"
#include "mpi.h"

#define MPI_SAFE_CALL( call ) do {                               \
    int err = call;                                              \
    if (err != MPI_SUCCESS) {                                    \
        fprintf(stderr, "MPI error %d in file '%s' at line %i",  \
               err, __FILE__, __LINE__);                         \
        exit(1);                                                 \
    } } while(0)

double norms (TwoLayerNet &nn) {
	double norm_sum = 0;

	for (int i = 0; i < nn.num_layers; ++i)  {
		norm_sum += arma::accu (arma::square (nn.W[i]));
	}

	return norm_sum;
}

/*
 * Computes the Cross-Entropy loss function for the neural network.
 */

double loss (TwoLayerNet &nn, const arma::mat& yc, const arma::mat& y, double reg)
{
	int N = yc.n_rows;
	double ce_sum = -arma::accu (arma::log (yc.elem (arma::find (y == 1))));

	double data_loss = ce_sum / N;
	double reg_loss = 0.5 * reg * norms(nn);
	double loss = data_loss + reg_loss;
	return loss;
}

void feedforward (TwoLayerNet &nn, const arma::mat& X, struct cache& cache)
{
	cache.z.resize(2);
	cache.a.resize(2);

	// std::cout << W[0].n_rows << "\n";
	assert (X.n_cols == nn.W[0].n_cols);
	cache.X = X;
	int N = X.n_rows;

	arma::mat z1 = X * nn.W[0].t() + arma::repmat(nn.b[0], N, 1);
	cache.z[0] = z1;

	// std::cout << "Computing a1 " << "\n";
	arma::mat a1;
	sigmoid (z1, a1);
	cache.a[0] = a1;

	// std::cout << "Computing z2 " << "\n";
	assert (a1.n_cols == nn.W[1].n_cols);
	arma::mat z2 = a1 * nn.W[1].t() + arma::repmat(nn.b[1], N, 1);
	cache.z[1] = z2;

	// std::cout << "Computing a2 " << "\n";
	arma::mat a2;
	softmax (z2, a2);
	cache.a[1] = cache.yc = a2;

}

/*
 * Computes the gradients of the cost w.r.t each param.
 * MUST be called after feedforward since it uses the bpcache.
 * @params y : N x C one-hot row vectors
 * @params bpcache : Output of feedforward.
 * @params bpgrads: Returns the gradients for each param
 */

void backprop (TwoLayerNet &nn, const arma::mat& y, double reg, const struct cache& bpcache, struct grads& bpgrads)
{
	bpgrads.dW.resize(2);
	bpgrads.db.resize(2);
	int N = y.n_rows;

	// std::cout << "backprop " << bpcache.yc << "\n";
	arma::mat diff = (1.0 / N) * (bpcache.yc - y);

	bpgrads.dW[1] = diff.t() * bpcache.a[0] + reg * nn.W[1];
	bpgrads.db[1] = arma::sum (diff, 0);
	arma::mat da1 = diff * nn.W[1];

	arma::mat dz1 = da1 % bpcache.a[0] % (1 - bpcache.a[0]);

	bpgrads.dW[0] = dz1.t() * bpcache.X + reg * nn.W[0];
	bpgrads.db[0] = arma::sum(dz1, 0);

}


/*
 * Returns a vector of labels for each row vector in the input
 */
void predict (TwoLayerNet &nn, const arma::mat& X, arma::mat& label)
{
	struct cache fcache;
	feedforward (nn, X, fcache);
	label.set_size (X.n_rows);

	for (int i = 0; i < X.n_rows; ++i) {
		arma::uword row, col;
		fcache.yc.row(i).max (row, col);
		label(i) = col;
	}
}


/*
 * Train the neural network &nn
 */
void train (TwoLayerNet &nn, const arma::mat& X, const arma::mat& y, double learning_rate, double reg, 
    const int epochs, const int batch_size, bool grad_check, int print_every)
{
	int N = X.n_rows;
	int iter = 0;

	for (int epoch = 0 ; epoch < epochs; ++epoch) {
		int num_batches = (int) ceil ( N / (float) batch_size);    

		for (int batch = 0; batch < num_batches; ++batch) {
			int last_row = std::min ((batch + 1)*batch_size-1, N-1);
			arma::mat X_batch = X.rows (batch * batch_size, last_row);
			arma::mat y_batch = y.rows (batch * batch_size, last_row);

			struct cache bpcache;
			feedforward (nn, X_batch, bpcache);

			struct grads bpgrads;
			backprop (nn, y_batch, reg, bpcache, bpgrads);

			if (print_every > 0 && iter % print_every == 0) {
				std::cout << "Loss at iteration " << iter << " of epoch " << epoch << "/" << epochs << " = " << loss (nn, bpcache.yc, y_batch, reg) << "\n";
			}

			// Gradient descent step
			for (int i = 0; i < nn.W.size(); ++i) {
				nn.W[i] -= learning_rate * bpgrads.dW[i];
			}

			for (int i = 0; i < nn.b.size(); ++i) {
				nn.b[i] -= learning_rate * bpgrads.db[i];
			}

			iter++;
		}
	}    
}

/*-----------------------------------------------------------------------------
 *  			
 *			OPTIMIZED FOR JUN 10
 *
 *-----------------------------------------------------------------------------*/

/*
 * Computes the Cross-Entropy loss function for the neural network.
 */

double loss_Jun10 (STwoLayerNet &nn, smat& yc, smat& y, double reg)
{
	double nor = 0;
	for(int i = 0; i<nn.W.size(); i++)
		nor += nn.W[i].norm();
	return (0.5*reg*nor+ smat::CE(yc,y)/yc.n_rows);
}


void par_train_Jun10_withMPI (STwoLayerNet &nn, smat& X, smat& y, double learning_rate, double reg, 
    const int epochs, const int batch_size, bool grad_check, int print_every)
{

	#ifdef _prof_mpi
	double t1 = MPI_Wtime();
	double reducetime =0;
	double d2h2dtime = 0;
	double scattertime = 0;
	double GPUtime = 0;
	double FFBPtime= 0;
	double netwkupdtime= 0;
	double losscomptime = 0;
	double mpitime = 0;
	double primetime = 0;
	double CPUtime =0;
	#endif
	int rank, num_procs;
	MPI_SAFE_CALL (MPI_Comm_size (MPI_COMM_WORLD, &num_procs));
	MPI_SAFE_CALL (MPI_Comm_rank (MPI_COMM_WORLD, &rank));

	int N = (rank == 0)?X.n_rows:0;
	MPI_SAFE_CALL (MPI_Bcast (&N, 1, MPI_INT, 0, MPI_COMM_WORLD));

	#ifdef _prof_mpi
	double t1_0 = MPI_Wtime();
	#endif

	/*-----------------------------------------------------------------------------
	 *  Scatter all batches to locally stored vectors
	 *-----------------------------------------------------------------------------*/
	int iter = 0;
	std::vector<smat> X_batches;
	std::vector<smat> y_batches;

	// prepare a vector of local strided batches.
	int num_batches = (int) ceil ( N / (float) batch_size);
	for(int batch = 0; batch<num_batches; ++batch){
		int last_row = std::min( (batch+1)*batch_size-1, N-1);
		int real_batch_size = last_row - batch * batch_size + 1;

		// subdivide batch and scatter
		smat X_Bigbatch(1,1,'h');
		smat y_Bigbatch(1,1,'h');
		if(rank == 0){
			X_Bigbatch.copy(real_batch_size,X.n_cols,&X.memptr()[batch_size*batch*X.n_cols],'h','h');
			y_Bigbatch.copy(real_batch_size,y.n_cols,&y.memptr()[batch_size*batch*y.n_cols],'h','h');
		}

		// bcast size of batch
		int X_ncols = (rank==0)?X_Bigbatch.n_cols:0; 
		int y_ncols = (rank==0)?y_Bigbatch.n_cols:0;

		#ifdef _prof_mpi
		double t1_1 = MPI_Wtime();
		#endif

		MPI_SAFE_CALL (MPI_Bcast (&X_ncols, 1, MPI_INT, 0, MPI_COMM_WORLD));
		MPI_SAFE_CALL (MPI_Bcast (&y_ncols, 1, MPI_INT, 0, MPI_COMM_WORLD));

		#ifdef _prof_mpi
		double t1_2 =MPI_Wtime();
		#endif

		// number of images in batch may not be divisible by 4;
		int sendrows[num_procs];
		int displrows[num_procs];
		int sendctX[num_procs];
		int displctX[num_procs];
		int sendcty[num_procs];
		int displcty[num_procs];
		
		// complete batch may not be available
		int pproc = (real_batch_size+num_procs-1)/num_procs;
		displrows[0] = 0;
		for(int p = 0; p<num_procs-1; p++){
			sendrows[p] = pproc;
			displrows[p+1] = (1+p)*pproc;
		}
		sendrows[num_procs-1] = real_batch_size-(num_procs-1)*pproc;
		int recvrows = sendrows[rank];
		// register on host
		smat X_batch_loc(recvrows,X_ncols,'h',true);
		smat y_batch_loc(recvrows,y_ncols,'h',true);

		for(int p = 0; p<num_procs; p++){
			sendctX[p] = sendrows[p]*X_ncols;
			sendcty[p] = sendrows[p]*y_ncols;
			displctX[p] = displrows[p]*X_ncols;
			displcty[p] = displrows[p]*y_ncols;
		}

		#ifdef _prof_mpi
		double t1_3 =MPI_Wtime();
		#endif

		MPI_SAFE_CALL(MPI_Scatterv( X_Bigbatch.memptr(), sendctX, displctX, MPI_DOUBLE, X_batch_loc.memptr(), X_batch_loc.n_rows*X_batch_loc.n_cols, MPI_DOUBLE,0, MPI_COMM_WORLD));
		MPI_SAFE_CALL(MPI_Scatterv( y_Bigbatch.memptr(), sendcty, displcty, MPI_DOUBLE, y_batch_loc.memptr(), y_batch_loc.n_rows*y_batch_loc.n_cols, MPI_DOUBLE,0, MPI_COMM_WORLD));

		#ifdef _prof_mpi
		double t1_4 =MPI_Wtime();
		scattertime += (t1_4 - t1_3 + t1_2 - t1_1 + t1_0 - t1);
		#endif

		X_batches.push_back(X_batch_loc);
		y_batches.push_back(y_batch_loc);

	}

	#ifdef _prof_mpi
	double t2 = MPI_Wtime();
	primetime = t2- t1;	
	#endif

	/*-----------------------------------------------------------------------------
	 *  Loop over all batches for each epoch and train
	 *-----------------------------------------------------------------------------*/
	//allocate "internal state" of the trainer
	struct sgrads nngrads, g_nn, l_nn;
	nngrads.dW.resize(nn.W.size());
	nngrads.db.resize(nn.W.size());
	nngrads.dz.resize(nn.W.size());
	g_nn.dW.resize(nn.W.size());
	g_nn.db.resize(nn.W.size());
	l_nn.dW.resize(nn.W.size());
	l_nn.db.resize(nn.W.size());
	for(int i = 0; i<nn.W.size(); i++){
		nngrads.dW[i].alloc(nn.W[i].n_rows,nn.W[i].n_cols,'d');
		nngrads.db[i].alloc(nn.b[i].n_rows,nn.b[i].n_cols,'d');
		l_nn.dW[i].alloc(nn.W[i].n_rows,nn.W[i].n_cols,'h',true);
		g_nn.dW[i].alloc(nn.W[i].n_rows,nn.W[i].n_cols,'h',true);
		l_nn.db[i].alloc(nn.b[i].n_rows,nn.b[i].n_cols,'h',true);
		g_nn.db[i].alloc(nn.b[i].n_rows,nn.b[i].n_cols,'h',true);
	}
	struct scache nncache;
	nncache.z.resize(nn.W.size());
	nncache.a.resize(nn.W.size());
	nncache.B.resize(nn.W.size());

	cudaStream_t layers[nn.W.size()];
	for(int i = 0; i<nn.W.size(); i++)
		cudaStreamCreate(&layers[i]);

	for (int epoch = 0 ; epoch < epochs; ++epoch) {
		for (int batch = 0; batch < num_batches; ++batch) {
			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t3 = MPI_Wtime();
			#endif
			
			//copy to device
			nncache.X.copy(X_batches[batch].n_rows,X_batches[batch].n_cols,X_batches[batch].memptr(),'d','h',false,true,'a',&layers[0]);	
			nncache.y.copy(y_batches[batch].n_rows,y_batches[batch].n_cols,y_batches[batch].memptr(),'d','h',false,true,'a',&layers[1]);
			cudaDeviceSynchronize();

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t4 = MPI_Wtime();
			#endif

			// feed forward and backpropagate
			parfeedforward_Jun10 (nn, nncache, layers);
			parbackprop_Jun10 (nn, reg*num_procs, nncache, nngrads, layers);

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t5 = MPI_Wtime();
			#endif
		
			double los;
			if(print_every > 0 && iter % print_every == 0)
				los = loss_Jun10 (nn, nncache.yc, nncache.y, reg*num_procs);

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t5_1 = MPI_Wtime();
			#endif

			double totalloss = 0;
			if(print_every > 0 && iter % print_every == 0){
				MPI_SAFE_CALL( MPI_Allreduce(&los,&totalloss,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD) );
				if(rank==0)
					std::cout << "Loss at iteration " << iter << " at epoch " << epoch << "/" << epochs << " = " << totalloss/num_procs << "\n";
			}

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t6 = MPI_Wtime();
			#endif

			// All procs performs the gradient descent
			for (int i = 0; i < nn.W.size(); ++i) {
				smat::axpy(-learning_rate/num_procs,nngrads.dW[i],(1./num_procs),nn.W[i]);
				smat::axpy(-learning_rate/num_procs,nngrads.db[i],(1./num_procs),nn.b[i]);
			}

			cudaDeviceSynchronize();
			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t7 = MPI_Wtime();
			#endif

			// Allreduce updates
			// copy to host
			for(int i = 0; i<nngrads.dW.size(); i++){
				l_nn.dW[i].copy(nn.W[i].n_rows,nn.W[i].n_cols,nn.W[i].memptr(),'h','d',true,false,'a',&layers[i]);
				l_nn.db[i].copy(1,nn.b[i].n_cols,nn.b[i].memptr(),'h','d',true,false,'a',&layers[i]);
			}
			cudaDeviceSynchronize();

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t8 = MPI_Wtime();
			#endif

			// comm
			for (int i = 0; i < nn.W.size(); ++i) {
				MPI_SAFE_CALL( MPI_Allreduce(l_nn.dW[i].memptr(), g_nn.dW[i].memptr(), nn.W[i].n_cols*nn.W[i].n_rows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD) );
				MPI_SAFE_CALL( MPI_Allreduce(l_nn.db[i].memptr(), g_nn.db[i].memptr(), nn.b[i].n_cols*nn.b[i].n_rows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD) );
			}
			cudaDeviceSynchronize();

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t9 = MPI_Wtime();
			#endif

			// copy to dev
			for(int i = 0; i<nngrads.dW.size(); i++){
				nn.W[i].copy(nn.W[i].n_rows,nn.W[i].n_cols,g_nn.dW[i].memptr(),'d','h',true,false,'a',&layers[i]);
				nn.b[i].copy(1,nn.b[i].n_cols,g_nn.db[i].memptr(),'d','h',true,false,'a',&layers[i]);
			}
			cudaDeviceSynchronize();
			iter++;

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t10 = MPI_Wtime();
			reducetime += (t9-t8)+(t6-t5_1);
			d2h2dtime += (t10-t7) - (t9-t8) + (t4-t3);
			losscomptime += (t5_1-t5);
			FFBPtime += (t5-t4);
			GPUtime += (t5-t4) + (t7-t6) + (t5_1-t5);
			netwkupdtime+= (t7-t6);
			#endif

		}
	}    
	for(int i = 0; i<nn.W.size(); i++)
		cudaStreamDestroy(layers[i]);
	cudaDeviceSynchronize();
	#ifdef _prof_mpi
	mpitime += reducetime + scattertime;
	CPUtime = primetime - scattertime;
	if(rank == 0){
		std::cout<<"======================================================================="<<std::endl;
		std::cout<<"Runtime information for Rank 0 processor:"<<std::endl;
		std::cout<<"======================================================================="<<std::endl;
		std::cout<<"CPU:"<<std::endl;
		std::cout<<"---------------------"<<std::endl;
		std::cout<<"took "<<primetime<<"s for creating "<<num_batches<<" batches, @ "<<primetime/num_batches<<"s/batch"<<std::endl;
		std::cout<<"took "<<CPUtime<<"s of CPU time only"<<std::endl<<std::endl;

		std::cout<<"MPI:"<<std::endl;
		std::cout<<"---------------------"<<std::endl;
		std::cout<<"took "<<scattertime<<"s for "<<num_batches<<" mpi_scatter, @ "<<scattertime/num_batches<<"s/batch"<<std::endl;
		std::cout<<"took "<<reducetime<<"s for "<<iter<<" mpi_allreduces, @ "<<reducetime/iter<<"s/iter"<<std::endl;
		std::cout<<"took "<<mpitime<<"s for all mpi comms"<<std::endl<<std::endl;

		std::cout<<"GPU<->CPU:"<<std::endl;
		std::cout<<"---------------------"<<std::endl;
		std::cout<<"took "<<d2h2dtime<<"s for "<<iter<<" copies GPU<->CPU, @ "<<d2h2dtime/iter<<"s/iter"<<std::endl<<std::endl;

		std::cout<<"GPU:"<<std::endl;
		std::cout<<"---------------------"<<std::endl;
		std::cout<<"took "<<losscomptime<<"s for "<<iter/print_every+1<<" evaluations of loss, @ "<<losscomptime/(iter/print_every+1)<<"s/print"<<std::endl;
		std::cout<<"took "<<FFBPtime<<"s for "<<iter<<" ff's and bp's, @ "<<FFBPtime/iter<<"s/iter"<<std::endl;
		std::cout<<"took "<<netwkupdtime<<"s for "<<iter<<" network updates, @ "<<netwkupdtime/iter<<"s/iter"<<std::endl;
		std::cout<<"took "<<GPUtime<<"s on GPU for "<<iter<<" iteration, @ "<<GPUtime/iter<<"s/iter"<<std::endl<<std::endl;
		std::cout<<"======================================================================="<<std::endl;
	}
	#endif
}

void parfeedforward_Jun10 (STwoLayerNet &nn, struct scache& cache, cudaStream_t* layers)
{
	// set sizes of cache vars
	cache.z[0].alloc(cache.X.n_rows,nn.H[1],'d');	
	cache.z[1].alloc(cache.X.n_rows,nn.H[2],'d');	
	cache.a[0].alloc(cache.X.n_rows,nn.H[1],'d');	
	cache.a[1].alloc(cache.X.n_rows,nn.H[2],'d');	

	cache.B[0].repmat(nn.b[0],cache.X.n_rows,&layers[0]);
	cache.B[1].repmat(nn.b[1],cache.X.n_rows,&layers[1]);

	// a1 = f1(z1 = X * W1^T + b1)
	smat::gemm(cache.z[0],1.,cache.X,nn.W[0],1.,cache.B[0],0);
	smat::sig(cache.a[0],cache.z[0]);
	// a2 = f2(z2 = a1 * W2^T + b2)
	smat::gemm(cache.z[1],1.,cache.a[0],nn.W[1],1.,cache.B[1],0);
	smat::sf(cache.a[1],cache.z[1]);

	cache.yc.copy(cache.a[1].n_rows,cache.a[1].n_cols,cache.a[1].memptr(),'d','d');

}

void parbackprop_Jun10 (STwoLayerNet &nn, double reg, struct scache& nncache, struct sgrads& nngrads, cudaStream_t* layers)
{
	double frac = 1./nncache.y.n_rows;;
	
	// allocate intermediate vars on device
	nngrads.dz[1].alloc(nncache.y.n_rows,nncache.y.n_cols,'d');
	nngrads.dz[0].alloc(nncache.y.n_rows,nn.W[1].n_cols,'d');

	smat::axpby(nngrads.dz[1],frac,nncache.yc,-frac,nncache.y);
	smat::gemm(nngrads.dz[0],1.,nngrads.dz[1],nn.W[1],1.,nncache.a[0],2,&layers[0]);

	// layer 2: dW2, db2
	smat::gemm(nngrads.dW[1],1.,nngrads.dz[1],nncache.a[0],reg,nn.W[1],1,&layers[1]);
	smat::reduce(nngrads.db[1],nngrads.dz[1],&layers[1]);

	// layer 1: dW1, db1
	smat::gemm(nngrads.dW[0],1.,nngrads.dz[0],nncache.X,reg,nn.W[0],1,&layers[0]);
	smat::reduce(nngrads.db[0],nngrads.dz[0],&layers[0]);
	
	cudaDeviceSynchronize();
}


/*
 * Returns a vector of labels for each row vector in the input
 */
void parpredict_Jun10 (TwoLayerNet &nn, arma::mat& X, arma::mat& label)
{
	struct cache fcache;
	parfeedforward_Jun1 (nn, X, fcache);
	label.set_size (X.n_rows);

	for (int i = 0; i < X.n_rows; ++i) {
		arma::uword row, col;
		fcache.yc.row(i).max (row, col);
		label(i) = col;
	}
}

/*-----------------------------------------------------------------------------
 *  
 *			Basic Parallel Implementation for Jun 1
 *
 *-----------------------------------------------------------------------------*/

void parfeedforward_Jun1 (TwoLayerNet &nn, arma::mat& X, struct cache& cache)
{

	double alpha = 1;
	double beta = 1;
	int type = 0;

	cache.z.resize(2);
	cache.a.resize(2);

	// z1 = z[0]
	assert (X.n_cols == nn.W[0].n_cols);
	cache.X = X;
	int N = X.n_rows;
	arma::mat B1 = arma::repmat(nn.b[0],N,1);
	cache.z[0] = B1;
	DoGEMM_Jun1(cache.z[0].memptr(),X.memptr(),nn.W[0].memptr(),B1.memptr(),alpha,beta,B1.n_rows, B1.n_cols, X.n_cols,type);

	// a[0] = a1 = sig(z1) = sig(z[0])
	sigmoid (cache.z[0], cache.a[0]);

	// z2 = z[1]
	assert (cache.z[0].n_cols == nn.W[1].n_cols);
	arma::mat B2 = arma::repmat(nn.b[1],N,1);
	cache.z[1] = B2;
	DoGEMM_Jun1(cache.z[1].memptr(),cache.a[0].memptr(),nn.W[1].memptr(),B2.memptr(),alpha,beta,B2.n_rows,B2.n_cols,cache.a[0].n_cols,type);

	// a[1] = a2 = sf(z2) = sf(z[1])
	softmax (cache.z[1], cache.yc);
	cache.a[1] = cache.yc;

}

/*
 * Computes the gradients of the cost w.r.t each param.
 * MUST be called after feedforward since it uses the bpcache.
 * @params y : N x C one-hot row vectors
 * @params bpcache : Output of feedforward.
 * @params bpgrads: Returns the gradients for each param
 */
void parbackprop_Jun1 (TwoLayerNet &nn, arma::mat& y, double reg, struct cache& bpcache, struct grads& bpgrads)
{
	bpgrads.dW.resize(2);
	bpgrads.db.resize(2);
	int N = y.n_rows;
	double alpha = 1;
	double beta = reg;
	int type = 1;

	// std::cout << "backprop " << bpcache.yc << "\n";
	arma::mat diff = (1.0 / N) * (bpcache.yc - y);
	// dW2
	bpgrads.dW[1] = nn.W[1];
	DoGEMM_Jun1(bpgrads.dW[1].memptr(),diff.memptr(),bpcache.a[0].memptr(),nn.W[1].memptr(),alpha,beta,nn.W[1].n_rows,nn.W[1].n_cols,diff.n_rows,type);
	bpgrads.db[1] = arma::sum (diff, 0);

	arma::mat dz1 = arma::zeros<arma::mat>(diff.n_rows,nn.W[1].n_cols);
	DoGEMM_Jun1(dz1.memptr(),diff.memptr(),nn.W[1].memptr(),bpcache.a[0].memptr(),1,1,dz1.n_rows,dz1.n_cols,diff.n_cols,2);

	// dW1
	bpgrads.dW[0] = nn.W[0];
	DoGEMM_Jun1(bpgrads.dW[0].memptr(),dz1.memptr(),bpcache.X.memptr(),nn.W[0].memptr(),alpha,beta,nn.W[0].n_rows,nn.W[0].n_cols,dz1.n_rows,type);
	bpgrads.db[0] = arma::sum(dz1, 0);
}


/*
 * Returns a vector of labels for each row vector in the input
 */
void parpredict_Jun1 (TwoLayerNet &nn, arma::mat& X, arma::mat& label)
{
	struct cache fcache;
	parfeedforward_Jun1 (nn, X, fcache);
	label.set_size (X.n_rows);

	for (int i = 0; i < X.n_rows; ++i) {
		arma::uword row, col;
		fcache.yc.row(i).max (row, col);
		label(i) = col;
	}
}

void par_train_Jun1_withMPI (TwoLayerNet &nn, const arma::mat& X, const arma::mat& y, double learning_rate, double reg, 
    const int epochs, const int batch_size, bool grad_check, int print_every)
{	
	#ifdef _prof_mpi
	double t1 = MPI_Wtime();
	double reducetime =0;
	double d2h2dtime = 0;
	double scattertime = 0;
	double netwkupdtime =0;
	double FFBPtime = 0;
	double losscomptime = 0;
	double mpitime = 0;
	double primetime = 0;
	double CPUtime = 0;
	#endif

	int rank, num_procs;
	MPI_SAFE_CALL (MPI_Comm_size (MPI_COMM_WORLD, &num_procs));
	MPI_SAFE_CALL (MPI_Comm_rank (MPI_COMM_WORLD, &rank));

	int N = (rank == 0)?X.n_rows:0;
	MPI_SAFE_CALL (MPI_Bcast (&N, 1, MPI_INT, 0, MPI_COMM_WORLD));
	
	#ifdef _prof_mpi
	double t2 = MPI_Wtime();
	mpitime+=(t2-t1);
	#endif

	int iter = 0;

	for (int epoch = 0 ; epoch < epochs; ++epoch) {
		int num_batches = (int) ceil ( N / (float) batch_size);
	
		for (int batch = 0; batch < num_batches; ++batch) {
			
			#ifdef _prof_mpi
			double t3 = MPI_Wtime();
			#endif

			int last_row = std::min ((batch + 1)*batch_size-1, N-1);
			int real_batch_size = last_row - batch * batch_size + 1;
			
			// subdivide batch and scatter
			arma::mat X_Bigbatch;
			arma::mat y_Bigbatch;
			if(rank == 0){
				X_Bigbatch = X.rows (batch * batch_size, last_row);
				y_Bigbatch = y.rows (batch * batch_size, last_row);
				// transpose to have test images contiguous
				X_Bigbatch = X_Bigbatch.t();
				y_Bigbatch = y_Bigbatch.t();
			}else{
			// simply allocate otherwise
				X_Bigbatch = arma::zeros<arma::mat>(nn.H[0], real_batch_size);
				y_Bigbatch = arma::zeros<arma::mat>(nn.H[2], real_batch_size);
			}

			// bcast size of batch
			int X_ncols = (rank==0)?X_Bigbatch.n_rows:0; 
			int y_ncols = (rank==0)?y_Bigbatch.n_rows:0;

			#ifdef _prof_mpi
			double t4 = MPI_Wtime();
			#endif

			MPI_SAFE_CALL (MPI_Bcast (&X_ncols, 1, MPI_INT, 0, MPI_COMM_WORLD));
			MPI_SAFE_CALL (MPI_Bcast (&y_ncols, 1, MPI_INT, 0, MPI_COMM_WORLD));

			#ifdef _prof_mpi
			double t5 = MPI_Wtime();
			#endif

			// number of images in batch may not be divisible by 4;
			int sendrows[num_procs];
			int displrows[num_procs];
			int sendctX[num_procs];
			int displctX[num_procs];
			int sendcty[num_procs];
			int displcty[num_procs];
			
			// complete batch may not be available
			int pproc = (real_batch_size+num_procs-1)/num_procs;
			displrows[0] = 0;
			for(int p = 0; p<num_procs-1; p++){
				sendrows[p] = pproc;
				displrows[p+1] = (1+p)*pproc;
			}
			sendrows[num_procs-1] = real_batch_size-(num_procs-1)*pproc;
			int recvrows = sendrows[rank];
			arma::mat X_batch = arma::zeros<arma::mat>(X_ncols,recvrows);
			arma::mat y_batch = arma::zeros<arma::mat>(y_ncols,recvrows);

			for(int p = 0; p<num_procs; p++){
				sendctX[p] = sendrows[p]*X_ncols;
				sendcty[p] = sendrows[p]*y_ncols;
				displctX[p] = displrows[p]*X_ncols;
				displcty[p] = displrows[p]*y_ncols;
			}

			#ifdef _prof_mpi
			double t6 = MPI_Wtime();
			#endif

			MPI_SAFE_CALL(MPI_Scatterv( X_Bigbatch.memptr(), sendctX, displctX, MPI_DOUBLE, X_batch.memptr(), X_batch.n_rows*X_batch.n_cols, MPI_DOUBLE,0, MPI_COMM_WORLD));
			MPI_SAFE_CALL(MPI_Scatterv( y_Bigbatch.memptr(), sendcty, displcty, MPI_DOUBLE, y_batch.memptr(), y_batch.n_rows*y_batch.n_cols, MPI_DOUBLE,0, MPI_COMM_WORLD));

			#ifdef _prof_mpi
			double t7 = MPI_Wtime();
			#endif

			// transpose back
			X_batch = X_batch.t();
			y_batch = y_batch.t();
			
			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t8 = MPI_Wtime();
			#endif

			struct cache bpcache;
			parfeedforward_Jun1 (nn, X_batch, bpcache);

			struct grads bpgrads;
			parbackprop_Jun1 (nn, y_batch, reg*num_procs, bpcache, bpgrads);

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t9 = MPI_Wtime();
			#endif

			// global storage
			struct grads gbpgrads;
			gbpgrads.dW.resize(nn.W.size());
			gbpgrads.db.resize(nn.W.size());
			for(int i =0; i<nn.W.size(); i++){
				gbpgrads.dW[i] = arma::zeros<arma::mat>(bpgrads.dW[i].n_rows,bpgrads.dW[i].n_cols);
				gbpgrads.db[i] = arma::zeros<arma::mat>(bpgrads.db[i].n_rows,bpgrads.db[i].n_cols);
			}
			
			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t10 = MPI_Wtime();
			#endif
		
			double los = 0, totalloss = 0;
			if(print_every>0 && iter%print_every == 0)
				los = loss (nn, bpcache.yc, y_batch, reg*num_procs);

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t10_1 = MPI_Wtime();
			#endif


			if(print_every>0 && iter%print_every == 0){
				MPI_SAFE_CALL( MPI_Allreduce(&los,&totalloss,1,MPI_DOUBLE,MPI_SUM,MPI_COMM_WORLD) );
				if(rank==0)
					std::cout << "Loss at iteration " << iter << " at epoch " << epoch << "/" << epochs << " = " << totalloss/num_procs << "\n";
			}

			#ifdef _prof_mpi
			double t11 = MPI_Wtime();
			#endif

			// Allreduce gradients
			for (int i = 0; i < nn.W.size(); ++i) {
				MPI_SAFE_CALL( MPI_Allreduce(bpgrads.dW[i].memptr(), gbpgrads.dW[i].memptr(), bpgrads.dW[i].n_cols*bpgrads.dW[i].n_rows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD) );
				MPI_SAFE_CALL( MPI_Allreduce(bpgrads.db[i].memptr(), gbpgrads.db[i].memptr(), bpgrads.db[i].n_cols*bpgrads.db[i].n_rows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD) );
			}

			#ifdef _prof_mpi
			double t12 = MPI_Wtime();
			#endif

			// All procs performs the gradient descent
			for (int i = 0; i < nn.W.size(); ++i) {
				nn.W[i] -= learning_rate * gbpgrads.dW[i]/num_procs;
			}

			for (int i = 0; i < nn.b.size(); ++i) {
				nn.b[i] -= learning_rate * gbpgrads.db[i]/num_procs;
			}

			#ifdef _prof_mpi
			cudaDeviceSynchronize();
			double t13 = MPI_Wtime();
			netwkupdtime+=(t13-t12);
			reducetime+=(t12-t11) + (t11-t10_1);
			losscomptime+=(t10_1-t10);
			d2h2dtime+=(t10-t9);
			FFBPtime+=(t9-t8);
			CPUtime+=(t8-t7) + (t10_1-t10) + (t13-t12) + (t6-t5) + (t4-t3);
			primetime+=(t8-t7) + (t6-t5) + (t4-t3);
			scattertime+=(t7-t6);
			mpitime+=(t5-t4)+(t7-t6)+(t12-t11);
			#endif

			iter++;
		}
	}
	#ifdef _prof_mpi
	if(rank == 0){
		std::cout<<"======================================================================="<<std::endl;
		std::cout<<"Runtime information for Rank 0 processor:"<<std::endl;
		std::cout<<"======================================================================="<<std::endl;
		std::cout<<"CPU:"<<std::endl;
		std::cout<<"---------------------"<<std::endl;
		std::cout<<"took "<<primetime<<"s creating "<<iter<<" batches, @ "<<primetime/iter<<"s/iter"<<std::endl;
		std::cout<<"took "<<CPUtime<<"s of CPU time only"<<std::endl<<std::endl;

		std::cout<<"MPI:"<<std::endl;
		std::cout<<"---------------------"<<std::endl;
		std::cout<<"took "<<scattertime<<"s for "<<iter<<" mpi_scatter, @ "<<scattertime/iter<<"s/iter"<<std::endl;
		std::cout<<"took "<<reducetime<<"s for "<<iter<<" mpi_allreduces, @ "<<reducetime/iter<<"s/iter"<<std::endl;
		std::cout<<"took "<<mpitime<<"s for all mpi comms"<<std::endl<<std::endl;

		std::cout<<"CPU<->CPU:"<<std::endl;
		std::cout<<"---------------------"<<std::endl;
		std::cout<<"took "<<d2h2dtime<<"s for "<<iter<<" copies CPU<->CPU, @ "<<d2h2dtime/iter<<"s/iter"<<std::endl<<std::endl;

		std::cout<<"GPU: (with huge hidden copy costs)"<<std::endl;
		std::cout<<"---------------------"<<std::endl;
		std::cout<<"took "<<losscomptime<<"s for "<<iter/print_every+1<<" evaluations of loss, @ "<<losscomptime/(iter/print_every+1)<<"s/print"<<std::endl;
		std::cout<<"took "<<FFBPtime<<"s for "<<iter<<" ff's and bp's, @ "<<FFBPtime/iter<<"s/iter"<<std::endl;
		std::cout<<"took "<<netwkupdtime<<"s for "<<iter<<" network updates, @ "<<netwkupdtime/iter<<"s/iter"<<std::endl;
		std::cout<<"GPUtime obtained by timing only GEMM kernel and Memcpy time"<<std::endl<<std::endl;
		std::cout<<"======================================================================="<<std::endl;
	}
	#endif
}


/*-----------------------------------------------------------------------------
 *  debug utilies
 *-----------------------------------------------------------------------------*/
void printCacheAfterFF(struct cache& cache){
	std::cout<<std::setprecision(16);
	std::cout<<"z1[2,2]="<<cache.z[0](2,2)<<std::endl;
	std::cout<<"a1[2,2]="<<cache.a[0](2,2)<<std::endl;
	std::cout<<"z2[2,2]="<<cache.z[1](2,2)<<std::endl;
	std::cout<<"a2[2,2]="<<cache.a[1](2,2)<<std::endl;
	std::cout<<"yc[2,2]="<<cache.yc(2,2)<<std::endl;
	std::cout<<std::setprecision(6);
}

void printSCacheAfterFF(struct scache& cache){
	std::cout<<std::setprecision(16);
	std::cout<<"z1[2,2]="<<cache.z[0](2,2)<<std::endl;
	std::cout<<"a1[2,2]="<<cache.a[0](2,2)<<std::endl;
	std::cout<<"z2[2,2]="<<cache.z[1](2,2)<<std::endl;
	std::cout<<"a2[2,2]="<<cache.a[1](2,2)<<std::endl;
	std::cout<<"yc[2,2]="<<cache.yc(2,2)<<std::endl;
	std::cout<<std::setprecision(6);
}

void printGradsAfterBP(struct grads& grads){
	std::cout<<std::setprecision(16);
	std::cout<<"dW1[2,2]="<<grads.dW[0](2,2)<<std::endl;
	std::cout<<"db1[0,2]="<<grads.db[0](0,2)<<std::endl;
	std::cout<<"dW2[2,2]="<<grads.dW[1](2,2)<<std::endl;
	std::cout<<"db2[0,2]="<<grads.db[1](0,2)<<std::endl;
	std::cout<<std::setprecision(6);
}

void printSGradsAfterBP(struct sgrads& grads){
	std::cout<<std::setprecision(16);
	std::cout<<"dW1[2,2]="<<grads.dW[0](2,2)<<std::endl;
	std::cout<<"db1[0,2]="<<grads.db[0](0,2)<<std::endl;
	std::cout<<"dW2[2,2]="<<grads.dW[1](2,2)<<std::endl;
	std::cout<<"db2[0,2]="<<grads.db[1](0,2)<<std::endl;
	std::cout<<std::setprecision(6);
}

void printMatrix(arma::mat& matrix){
	std::cout<<"Matrix[2,2]= "<<matrix(2,2)<<std::endl;
}

void printMatrixDims(const arma::mat & matrix){
	std::cout<<matrix.n_rows<<" x "<<matrix.n_cols<<std::endl;
}

void printmsg(const std::string msg){
	std::cout<<msg<<std::endl;
}

void Break(std::string st){
	int procid, numprocs;
	MPI_Comm_rank(MPI_COMM_WORLD,&procid);
	MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
	if(procid==0)
		std::cout<<st<<std::endl;
	MPI_Barrier(MPI_COMM_WORLD);
	for(int p = 0; p<numprocs; p++){
		if(p == procid)
			std::cout<<"proc "<<p<<" is here"<<std::endl;
		MPI_Barrier(MPI_COMM_WORLD);
	}
	MPI_Barrier(MPI_COMM_WORLD);
}


