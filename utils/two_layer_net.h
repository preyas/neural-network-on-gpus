#ifndef UTILS_TWO_LAYER_NET_H_
#define UTILS_TWO_LAYER_NET_H_

#include <armadillo>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <cuda_runtime.h>
#include "../defs.h"
#include "../simple_mat.h"

class TwoLayerNet
{
public:
	const int num_layers = 2;
	// H[i] is the number of neurons in layer i (where i=0 implies input layer)
	std::vector<int> H;
	// Weights of the neural network
	// W[i] are the weights of the i^th layer
	std::vector<arma::mat> W;
	// Biases of the neural network
	// b[i] is the row vector biases of the i^th layer
	std::vector<arma::rowvec> b;

	TwoLayerNet (std::vector<int> _H) {
		W.resize (num_layers);
		b.resize (num_layers);
		H = _H;

		for (int i = 0; i < num_layers; i++) {
			arma::arma_rng::set_seed(arma::arma_rng::seed_type(i));
			W[i] = 0.0001 * arma::randn (H[i+1], H[i]);// 0.0001*arma::ones<arma::mat>(H[i+1], H[i]);
			b[i].zeros (H[i+1]);
		}
	}
};


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

class STwoLayerNet
{
public:
	const int num_layers = 2;
	// H[i] is the number of neurons in layer i (where i=0 implies input layer)
	std::vector<int> H;
	// Weights of the neural network
	// W[i] are the weights of the i^th layer
	std::vector<smat> W;
	// Biases of the neural network
	// b[i] is the row vector biases of the i^th layer
	std::vector<smat> b;
	char loc;

	STwoLayerNet (std::vector<int> _H, const char hd) {
		W.resize (num_layers);
		b.resize (num_layers);
		H = _H;
		if(hd!='h' && hd!='d')
			std::cout<<"input valid location where the class lives"<<std::endl;
		loc = hd;

		for (int i = 0; i < num_layers; i++) {
			arma::arma_rng::set_seed(arma::arma_rng::seed_type(i));
			arma::mat dummy = 0.0001 * arma::randn (H[i+1], H[i]);
			dummy = dummy.t();
			W[i].copy(dummy.n_cols,dummy.n_rows,dummy.memptr(),hd,'h');
			b[i].size(1,H[i+1],hd);
		}
	}

	STwoLayerNet (STwoLayerNet& source, const char hd) {
		W.resize (source.num_layers);
		b.resize (source.num_layers);
		H = source.H;
		if(hd!='h' && hd!='d')
			std::cout<<"input valid location where the class lives"<<std::endl;
		loc = hd;

		for (int i = 0; i < num_layers; i++) {
			W[i].copy(source.W[i].n_rows,source.W[i].n_cols,source.W[i].memptr(),hd,source.loc);
			b[i].copy(source.b[i].n_rows,source.b[i].n_cols,source.b[i].memptr(),hd,source.loc);
		}
	}
};

////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

/*-----------------------------------------------------------------------------
 * serial
 *-----------------------------------------------------------------------------*/
void feedforward (TwoLayerNet &nn, const arma::mat& X, struct cache& bpcache);
double loss (TwoLayerNet &nn, const arma::mat& yc, const arma::mat& y, double reg);
void backprop (TwoLayerNet &nn, const arma::mat& y, double reg, const struct cache& bpcache, struct grads& bpgrads);
void numgrad (TwoLayerNet &nn, const arma::mat& X, const arma::mat& y, double reg, struct grads& numgrads);
void train (TwoLayerNet &nn, const arma::mat& X, const arma::mat& y, double learning_rate, double reg = 0.0, const int epochs = 15, const int batch_size = 800, bool grad_check = false, int print_every = -1);
void predict (TwoLayerNet &nn, const arma::mat& X, arma::mat& label);


/*-----------------------------------------------------------------------------
 *  My debug utilities
 *-----------------------------------------------------------------------------*/
void printCacheAfterFF(struct cache& cache);
void printSCacheAfterFF(struct scache& cache);
void printGradsAfterBP(struct grads& grads);
void printSGradsAfterBP(struct sgrads& grads);
void printmsg(std::string);
void printMatrixDims(const arma::mat & matrix);

/*-----------------------------------------------------------------------------
 *  Jun 1
 *-----------------------------------------------------------------------------*/
void parfeedforward_Jun1 (TwoLayerNet &nn, arma::mat& X, struct cache& bpcache);
void parpredict_Jun1 (TwoLayerNet &nn, arma::mat& X, arma::mat& label);
void parbackprop_Jun1 (TwoLayerNet &nn, arma::mat& y, double reg, struct cache& bpcache, struct grads& bpgrads);

void par_train_Jun1 (TwoLayerNet &nn, const arma::mat& X, const arma::mat& y, double learning_rate, double reg = 0.0, const int epochs = 15, const int batch_size = 800, bool grad_check = false, int print_every = -1);

void par_train_Jun1_withMPI (TwoLayerNet &nn, const arma::mat& X, const arma::mat& y, double learning_rate, double reg = 0.0, const int epochs = 15, const int batch_size = 800, bool grad_check = false, int print_every = -1);


/*-----------------------------------------------------------------------------
 *  Jun 10 
 *-----------------------------------------------------------------------------*/
void par_train_Jun10_withMPI (STwoLayerNet &nn, smat& X, smat& y, double learning_rate, double reg = 0.0, const int epochs = 15, const int batch_size = 800, bool grad_check = false, int print_every = -1);
void parfeedforward_Jun10 (STwoLayerNet &nn, struct scache& bpcache,cudaStream_t* layers);
void parpredict_Jun10 (TwoLayerNet &nn, arma::mat& X, arma::mat& label);
void parbackprop_Jun10 (STwoLayerNet &nn, double reg, struct scache& bpcache, struct sgrads& bpgrads, cudaStream_t* layers);

double loss_Jun10 (STwoLayerNet &nn, smat& yc, smat& y, double reg);


#endif
